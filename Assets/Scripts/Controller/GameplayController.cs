﻿using System.Collections;
using UnityEngine;

using com.faithstudio.Gameplay;

public class GameplayController : MonoBehaviour
{
    #region Public Variables

    public static GameplayController Instance;

    public Sprite[] defaultSpriteContentToDecyle;

    [Space(5.0f)]
    [Header("Reference  :   External")]
    public CharacterControllerForDefence CharacterControllerForDefenderReference;
    public CharacterControllerForAttacker CharacterControllerForAttackerReference;

    [Space(5.0f)]
    public AIControllerForPaintDefender AIControllerForPaintDefenderReference;
    public AIControllerForPaintAttacker AIControllerForPaintAttackerReference;

    [Space(5.0f)]
    public LevelManager levelManagerReference;
    public ColorTurretController colorGunControllerReference;
    public CoinEarningController coinEarnControllerReference;

    #endregion

    #region Private Variables

    private bool m_IsGameRunning;

    private int m_PlayerIDForAttacker;
    private int m_PlayerIDForDefender;

    private double m_TotalCoinEarnedOnThisLevel;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        if (Instance == null)
        {

            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {

            Destroy(gameObject);
        }
    }

    private void Start()
    {
        UIStateController.Instance.AppearMainMenuPanel(levelManagerReference.GetCurrentLevel());
        GlobalTouchController.Instance.EnableTouchController();

    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForCheckingDurationOfRound(float t_DurationOfRound)
    {

        float t_CycleLength = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        float t_RemainingTimeForRound = t_DurationOfRound;
        float t_Progression;

        UIGameplayPanelController t_UIGameplayPanelControllerReference = UIStateController.Instance.UIGameplayControllerReference;

        while (t_RemainingTimeForRound > 0)
        {

            yield return t_CycleDelay;

            t_RemainingTimeForRound -= t_CycleLength;
            t_Progression = (t_RemainingTimeForRound / t_DurationOfRound);

            t_UIGameplayPanelControllerReference.UpdateUIForTimer(t_Progression);
        }

        StopCoroutine(ControllerForCheckingDurationOfRound(0));
    }

    private IEnumerator ControllerForPreProcess()
    {

        int t_CurrentLevel = levelManagerReference.GetCurrentLevel();
        float t_DurationForEachRound = levelManagerReference.GetDurationOfEachRoundForCurrentLevel();
        float t_LevelProgression = levelManagerReference.GetLevelProgression();

        UIStateController.Instance.DisappearMainMenuPanel();

        yield return new WaitForSeconds(0.5f);

        UIStateController.Instance.AppearGameplayPanel(false, 2);

        DecyleUsingSpriteMaskController.Instance.EnableSpriteSplatting(defaultSpriteContentToDecyle[Random.Range(0, defaultSpriteContentToDecyle.Length)]);

        coinEarnControllerReference.PreProcess(PlayerStateController.Instance.GetCurrentStateOfCoinEarn());

        //Round : PlayerDefending
        UIStateController.Instance.ChangeRoundTextInfo("DEFEND");
        m_PlayerIDForAttacker = 1;
        m_PlayerIDForDefender = 0;
        colorGunControllerReference.PreProcess(
            false,
            levelManagerReference.GetNumberOfColorGunForCurrentLevel());
        colorGunControllerReference.ChangeMessageInfoOfColorGun(3, "Bang!");
        CharacterControllerForDefenderReference.EnableCharacterControllerForDefender(true);
        CharacterControllerForAttackerReference.PreProcess();
        AIControllerForPaintAttackerReference.PreProcess(t_LevelProgression);

        yield return StartCoroutine(ControllerForCheckingDurationOfRound(t_DurationForEachRound));

        //StopEverything
        DecyleUsingSpriteMaskController.Instance.HideAllSplat();
        colorGunControllerReference.PostProcess();
        CharacterControllerForDefenderReference.DisableCharacterControllerForDefender();
        CharacterControllerForAttackerReference.PostProcess();
        AIControllerForPaintAttackerReference.PostProcess();

        //Delay BetweenRound
        yield return new WaitForSeconds(1f);

        UIStateController.Instance.ChangeRoundTextInfo("ATTACK");
        m_PlayerIDForAttacker = 0;
        m_PlayerIDForDefender = 1;
        colorGunControllerReference.PreProcess(
            true,
            levelManagerReference.GetNumberOfColorGunForCurrentLevel());
        colorGunControllerReference.ChangeMessageInfoOfColorGun(3, "Taap!");
        CharacterControllerForDefenderReference.EnableCharacterControllerForDefender(false);
        CharacterControllerForAttackerReference.PreProcess();
        AIControllerForPaintDefenderReference.PreProcess(t_LevelProgression);

        yield return StartCoroutine(ControllerForCheckingDurationOfRound(t_DurationForEachRound));

        DecyleUsingSpriteMaskController.Instance.HideAllSplat();
        colorGunControllerReference.PostProcess();
        CharacterControllerForDefenderReference.DisableCharacterControllerForDefender();
        CharacterControllerForAttackerReference.PostProcess();
        AIControllerForPaintDefenderReference.PostProcess();

        UIStateController.Instance.DisappearGameplayPanel(false);

        yield return new WaitForSeconds(0.5f);

        bool t_IsPlayerWin = false;
        if (UIStateController.Instance.GetPlayerRankInRankList(PlayerStateController.Instance.GetPlayerName()) == 0)
        {
            t_IsPlayerWin = true;
            levelManagerReference.IncreaseLevel();
        }


        UIStateController.Instance.AppearGameOverPanel(
            t_IsPlayerWin,
            coinEarnControllerReference.GetNumberOfCoinEarnedInThisLevel(),
            delegate
            {

                UIStateController.Instance.AppearMainMenuPanel(levelManagerReference.GetCurrentLevel());
                coinEarnControllerReference.PostProcess();
            });

        StopCoroutine(ControllerForPreProcess());
        m_IsGameRunning = false;
    }

    #endregion

    #region Public Callback

    public void PreProcess()
    {
        if (!m_IsGameRunning)
        {

            m_IsGameRunning = true;
            StartCoroutine(ControllerForPreProcess());
        }
    }

    public void PostProcess()
    {

        m_IsGameRunning = false;
    }

    public int GetPlayerIDForAttacker()
    {

        return m_PlayerIDForAttacker;
    }

    public int GetPlayerIDForDefender()
    {

        return m_PlayerIDForDefender;
    }

    public void EarnCoinToPlayer(bool t_IsDefendingCharater)
    {

        if (t_IsDefendingCharater)
        {

            coinEarnControllerReference.AddCoinToUser(
                    CharacterControllerForDefenderReference.transform.position,
                    Vector3.up,
                    Vector3.zero
                );
        }
        else
        {

            coinEarnControllerReference.AddCoinToUser(
                    CharacterControllerForAttackerReference.transform.position,
                    Vector3.up,
                    Vector3.zero
                );
        }
    }

    #endregion

}
