﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using com.faithstudio.Math;

public class AIControllerForPaintDefender : MonoBehaviour
{

    #region Custom Variables

    private enum AIState
    {
        defence,
        idle
    }

    #endregion

    #region Public Variables

    [Space(5.0f)]
    [Header("Reference  :   External")]
    public ColorTurretController colorGunControllerReference;
    public CharacterControllerForDefence characterControllerForDefenceReference;
    [Space(5.0f)]
    public LerpWaypointController lerpWaypointControllerForVertical;
    public LerpWaypointController lerpWaypointControllerForHorizontal;

    [Space(10.0f)]
    [Range(0f, 0.5f)]
    public float AITouchMovementSpeed;

    [Space(10.0f)]
    [Range(0f, 1f)]
    public float varientOfAIBehaviour;
    public AnimationCurve curveForVarientOfAIBehaviour;
    [Range(0.25f, 1f)]
    public float thinkingEfficiency;
    [Range(0f, 1f)]
    public float probabilityOfDefend;

    [Space(10.0f)]
    [Range(0f, 0.25f)]
    public float AIStateVariationThroughProgression;
    public AnimationCurve curveForAIStateVariationThroughProgression;

    #endregion

    #region Private Variables

    private AIState m_AIState;

    private bool m_IsAIControllerRunning;

    private float m_AIStateMultiplier;
    private float m_AbsoluteThinkingEfficieny;
    private float m_AbsoluteProbabilityOfDefend;


    #endregion

    #region Mono Behaviour

    #endregion

    #region Configuretion

    private Vector3 GetIdlePosition()
    {

        Vector3 t_VerticalMarkPosition = lerpWaypointControllerForVertical.GetPositionThroughLerpValue(Random.Range(0f, 1f));
        Vector3 t_HorizontalMarkPosition = lerpWaypointControllerForHorizontal.GetPositionThroughLerpValue(Random.Range(0f, 1f));

        Vector3 t_Result = new Vector3(
                t_HorizontalMarkPosition.x,
                t_VerticalMarkPosition.y,
                (t_VerticalMarkPosition.z + t_HorizontalMarkPosition.z) / 2.0f
            );

        return t_Result;
    }

    private IEnumerator ControllerForAI()
    {
        float t_CycleLength = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        Transform t_CharacterTransform = characterControllerForDefenceReference.transform;
        Transform t_VerticalMark = characterControllerForDefenceReference.verticalMark;
        Transform t_HorizontalMark = characterControllerForDefenceReference.horizontalMark;
        Transform t_TargetedColorBullet = t_CharacterTransform;

        Vector3 t_ModifiedPosition;
        Vector3 t_IdlePositionForVerticalMark = Vector3.zero;
        Vector3 t_IdlePositionForHorizontalMark = Vector3.zero;

        float t_RemainingTimeForThinkingCycle = 0f;


        float t_AbsoluteTouchMovementSpeed = m_AIStateMultiplier * AITouchMovementSpeed;

        while (m_IsAIControllerRunning)
        {

            if (t_RemainingTimeForThinkingCycle <= 0)
            {

                if (Random.Range(0f, 1f) <= m_AbsoluteProbabilityOfDefend)
                {
                    List<Transform> t_ColorBulletPosition = new List<Transform>(colorGunControllerReference.GetListOfActiveBullet());

                    int t_NumberOfColorBulletAvailableToDoge = t_ColorBulletPosition.Count;

                    if (t_NumberOfColorBulletAvailableToDoge > 0)
                    {
                        int t_NearestColorBulletIndex = 0;

                        float t_LowestDistance = Mathf.Infinity;
                        float t_CurrentDistance;

                        for (int colorBulletIndex = 0; colorBulletIndex < t_NumberOfColorBulletAvailableToDoge; colorBulletIndex++)
                        {

                            t_CurrentDistance = Vector3.Distance(t_CharacterTransform.position, t_ColorBulletPosition[colorBulletIndex].position);
                            if (t_CurrentDistance < t_LowestDistance)
                            {

                                t_NearestColorBulletIndex = colorBulletIndex;
                                t_LowestDistance = t_CurrentDistance;
                            }
                        }


                        t_TargetedColorBullet = t_ColorBulletPosition[t_NearestColorBulletIndex];

                        m_AIState = AIState.defence;
                    }
                    else
                    {

                        t_IdlePositionForVerticalMark = lerpWaypointControllerForVertical.GetPositionThroughLerpValue(Random.Range(0f, 1f));
                        t_IdlePositionForHorizontalMark = lerpWaypointControllerForHorizontal.GetPositionThroughLerpValue(Random.Range(0f, 1f));

                        m_AIState = AIState.idle;
                    }
                }
                else
                {
                    t_IdlePositionForVerticalMark = lerpWaypointControllerForVertical.GetPositionThroughLerpValue(Random.Range(0f, 1f));
                    t_IdlePositionForHorizontalMark = lerpWaypointControllerForHorizontal.GetPositionThroughLerpValue(Random.Range(0f, 1f));

                    m_AIState = AIState.idle;
                }

                t_RemainingTimeForThinkingCycle = m_AbsoluteThinkingEfficieny;
            }

            switch (m_AIState)
            {
                case AIState.defence:

                    t_ModifiedPosition = new Vector3(
                                t_VerticalMark.position.x,
                                t_TargetedColorBullet.position.y,
                                t_VerticalMark.position.z
                            );
                    t_ModifiedPosition = Vector3.Lerp(
                            t_VerticalMark.position,
                            t_ModifiedPosition,
                            lerpWaypointControllerForVertical.lerpingSpeed * t_AbsoluteTouchMovementSpeed
                        );
                    t_VerticalMark.position = t_ModifiedPosition;

                    t_ModifiedPosition = new Vector3(
                                t_TargetedColorBullet.position.x,
                                t_HorizontalMark.position.y,
                                t_HorizontalMark.position.z
                            );
                    t_ModifiedPosition = Vector3.Lerp(
                            t_HorizontalMark.position,
                            t_ModifiedPosition,
                            lerpWaypointControllerForHorizontal.lerpingSpeed * t_AbsoluteTouchMovementSpeed
                        );
                    t_HorizontalMark.position = t_ModifiedPosition;

                    break;
                case AIState.idle:

                    t_ModifiedPosition = Vector3.Lerp(
                            t_VerticalMark.position,
                            t_IdlePositionForVerticalMark,
                            lerpWaypointControllerForVertical.lerpingSpeed * t_AbsoluteTouchMovementSpeed
                        );
                    t_VerticalMark.position = t_ModifiedPosition;

                    t_ModifiedPosition = Vector3.Lerp(
                            t_HorizontalMark.position,
                            t_IdlePositionForHorizontalMark,
                            lerpWaypointControllerForHorizontal.lerpingSpeed * t_AbsoluteTouchMovementSpeed
                        );
                    t_HorizontalMark.position = t_ModifiedPosition;

                    break;
            }

            yield return t_CycleDelay;
            t_RemainingTimeForThinkingCycle -= t_CycleLength;
        }

        StopCoroutine(ControllerForAI());
    }

    #endregion

    #region Public Callback

    public void PreProcess(float t_LevelProgression)
    {

        m_AIState = AIState.idle;

        float t_LowerBoundOfAIStat = (1f - (AIStateVariationThroughProgression * 0.5f)) - ((AIStateVariationThroughProgression * 0.5f) * curveForAIStateVariationThroughProgression.Evaluate(1 - t_LevelProgression));
        float t_UpperBoundOfAIStat = 1 + (AIStateVariationThroughProgression * curveForAIStateVariationThroughProgression.Evaluate(t_LevelProgression));

        m_AIStateMultiplier = Random.Range(t_LowerBoundOfAIStat, t_UpperBoundOfAIStat);

        float t_LowerBoundForThinkingEfficieny = thinkingEfficiency - (0.5f * thinkingEfficiency * varientOfAIBehaviour * curveForVarientOfAIBehaviour.Evaluate(t_LevelProgression));
        t_LowerBoundForThinkingEfficieny = t_LowerBoundForThinkingEfficieny < 0 ? 0 : t_LowerBoundForThinkingEfficieny;

        float t_UpperBoundForThinkingEfficieny = thinkingEfficiency + (thinkingEfficiency * varientOfAIBehaviour * curveForVarientOfAIBehaviour.Evaluate(t_LevelProgression));
        t_UpperBoundForThinkingEfficieny = t_UpperBoundForThinkingEfficieny > 1 ? 1 : t_UpperBoundForThinkingEfficieny;

        m_AbsoluteThinkingEfficieny = Random.Range(t_LowerBoundForThinkingEfficieny, t_UpperBoundForThinkingEfficieny);

        float t_LowerBoundForProbabilityOfDefend = probabilityOfDefend - (0.5f * probabilityOfDefend * varientOfAIBehaviour * curveForVarientOfAIBehaviour.Evaluate(t_LevelProgression));
        t_LowerBoundForProbabilityOfDefend = t_LowerBoundForProbabilityOfDefend < 0 ? 0 : t_LowerBoundForProbabilityOfDefend;

        float t_UpperBoundForProbabilityOfDefend = probabilityOfDefend + (probabilityOfDefend * varientOfAIBehaviour * curveForVarientOfAIBehaviour.Evaluate(t_LevelProgression));
        t_UpperBoundForProbabilityOfDefend = t_UpperBoundForProbabilityOfDefend > 1 ? 1 : t_UpperBoundForProbabilityOfDefend;

        m_AbsoluteProbabilityOfDefend = Random.Range(t_LowerBoundForProbabilityOfDefend, t_UpperBoundForProbabilityOfDefend);

        if (!m_IsAIControllerRunning)
        {
            m_IsAIControllerRunning = true;
            StartCoroutine(ControllerForAI());
        }

        Debug.Log("(" + t_LowerBoundForProbabilityOfDefend + "," + t_UpperBoundForProbabilityOfDefend + ")");
        Debug.Log("AI_Defender : " + m_AIStateMultiplier + ", " + m_AbsoluteThinkingEfficieny + ", " + m_AbsoluteProbabilityOfDefend);
    }

    public void PostProcess()
    {

        m_IsAIControllerRunning = false;
    }

    #endregion
}
