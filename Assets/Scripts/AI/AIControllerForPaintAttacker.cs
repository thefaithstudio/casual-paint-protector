﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIControllerForPaintAttacker : MonoBehaviour
{

    #region Custom Variables

    private enum AIState
    {
        attack,
        idle
    }

    #endregion

    #region Public Variables

    [Space(10.0f)]
    [Header("Reference  :   External")]
    public ColorTurretController ColorGunControllerReference;
    public CharacterControllerForAttacker CharacterControllerForAttackerReference;

    [Space(5.0f)]
    [Range(0f, 1f)]
    public float varientOfAIBehaviour;
    public AnimationCurve curveForVarientOfAIBehaviour;
    [Range(0.25f, 1f)]
    public float thinkingEfficiency;
    [Range(0f, 1f)]
    public float probabilityOfAttack;

    [Space(10.0f)]
    [Range(0f, 0.25f)]
    public float AIStateVariationThroughProgression;
    public AnimationCurve curveForAIStateVariationThroughProgression;

    #endregion

    #region Private Variables

    private AIState m_AIState;

    private bool m_IsAIControllerRunning;

    private float m_AIStateMultiplier;
    private float m_AbsoluteThinkingEfficieny;
    private float m_AbsoluteProbabilityOfAttack;

    #endregion

    #region Mono Behaviour

    #endregion

    #region Configuretion

    private IEnumerator ControllerForAI()
    {

        float t_CycleLength = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);
        WaitForSeconds t_DelayForIdle = new WaitForSeconds(1f - m_AbsoluteThinkingEfficieny);

        List<ColorTurretAttribute> t_ActiveColorGunAttribute = ColorGunControllerReference.GetListOfActiveColorGunAttribute();
        int t_NumberOfActiveColorGun = t_ActiveColorGunAttribute.Count;
        int t_SelectedColorGunIndex;

        float t_RemainingTimeForThinkingCycle = 0f;

        while (m_IsAIControllerRunning)
        {

            if (t_RemainingTimeForThinkingCycle <= 0)
            {

                if (Random.Range(0f, 1f) <= m_AbsoluteProbabilityOfAttack)
                {
                    t_SelectedColorGunIndex = Random.Range(0, t_NumberOfActiveColorGun);
                    CharacterControllerForAttackerReference.SetTargetForCharacter(
                        t_ActiveColorGunAttribute[t_SelectedColorGunIndex]);

                    m_AIState = AIState.attack;
                }
                else
                {

                    m_AIState = AIState.idle;
                    yield return t_DelayForIdle;
                }

                t_RemainingTimeForThinkingCycle = m_AbsoluteThinkingEfficieny;
            }

            yield return t_CycleDelay;
            t_RemainingTimeForThinkingCycle -= t_CycleLength;
        }
    }


    #endregion

    #region Public Callback

    public void PreProcess(float t_LevelProgression)
    {

        m_AIState = AIState.idle;

        float t_LowerBoundOfAIStat = (1f - (AIStateVariationThroughProgression * 0.5f)) - ((AIStateVariationThroughProgression * 0.5f) * curveForAIStateVariationThroughProgression.Evaluate(1 - t_LevelProgression));
        float t_UpperBoundOfAIStat = 1 + (AIStateVariationThroughProgression * curveForAIStateVariationThroughProgression.Evaluate(t_LevelProgression));

        m_AIStateMultiplier = Random.Range(t_LowerBoundOfAIStat, t_UpperBoundOfAIStat);

        float t_LowerBoundForThinkingEfficieny = thinkingEfficiency - (0.5f * thinkingEfficiency * varientOfAIBehaviour * curveForVarientOfAIBehaviour.Evaluate(t_LevelProgression));
        t_LowerBoundForThinkingEfficieny = t_LowerBoundForThinkingEfficieny < 0 ? 0 : t_LowerBoundForThinkingEfficieny;

        float t_UpperBoundForThinkingEfficieny = thinkingEfficiency + (thinkingEfficiency * varientOfAIBehaviour * curveForVarientOfAIBehaviour.Evaluate(t_LevelProgression));
        t_UpperBoundForThinkingEfficieny = t_UpperBoundForThinkingEfficieny > 1 ? 1 : t_UpperBoundForThinkingEfficieny;

        m_AbsoluteThinkingEfficieny = Random.Range(t_LowerBoundForThinkingEfficieny, t_UpperBoundForThinkingEfficieny);

        float t_LowerBoundForProbabilityOfAttack = probabilityOfAttack - (0.5f * probabilityOfAttack * varientOfAIBehaviour * curveForVarientOfAIBehaviour.Evaluate(t_LevelProgression));
        t_LowerBoundForProbabilityOfAttack = t_LowerBoundForProbabilityOfAttack < 0 ? 0 : t_LowerBoundForProbabilityOfAttack;

        float t_UpperBoundForProbabilityOfAttack = probabilityOfAttack + (probabilityOfAttack * varientOfAIBehaviour * curveForVarientOfAIBehaviour.Evaluate(t_LevelProgression));
        t_UpperBoundForProbabilityOfAttack = t_UpperBoundForProbabilityOfAttack > 1 ? 1 : t_UpperBoundForProbabilityOfAttack;

        m_AbsoluteProbabilityOfAttack = Random.Range(t_LowerBoundForProbabilityOfAttack, t_UpperBoundForProbabilityOfAttack);

        Debug.Log("(" + t_LowerBoundForProbabilityOfAttack + "," + t_UpperBoundForProbabilityOfAttack + ")");
        Debug.Log("AI_Attacker : " + m_AIStateMultiplier + ", " + m_AbsoluteThinkingEfficieny + ", " + m_AbsoluteProbabilityOfAttack);

        if (!m_IsAIControllerRunning)
        {

            m_IsAIControllerRunning = true;
            StartCoroutine(ControllerForAI());
        }
    }

    public void PostProcess()
    {

        m_IsAIControllerRunning = false;
    }

    #endregion
}
