﻿using UnityEngine;
using com.faithstudio.GameplayService;

public class AudioController : MonoBehaviour
{

    #region Public Variables

    public static AudioController Instance;

    public AudioManager audioManager;
    public GeoLocationManager geoLocationManagerReference;

    [Space(5.0f)]
    public AudioClip bgmForMainMenu;

    [Space(5.0f)]
    public AudioClip buttonSelect;
    public AudioClip coinEarn;

    #endregion

    #region Private Variables

    private AudioClip m_CurrentAudioClipForLevel;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        PlayBackgroundMusicForMainMenu();
    }

    #endregion

    #region Configuretion

    #endregion

    #region Public Callback

    public void PlayBackgroundMusicForMainMenu()
    {

        if (!AudioManager.Instance.IsSoundAlreadyPlayingInAnyAudioSource(bgmForMainMenu))
            audioManager.PlaySound(bgmForMainMenu, true);
    }

    public void StopBackgroundMusicForMainMenu()
    {

        audioManager.StopSound(bgmForMainMenu);
    }

    public void PlayBackgroundMusicForGameplay(int t_CurrentLevel = 0)
    {

        AudioClip t_AudioClipForGameplay = geoLocationManagerReference.GetBackgroundMusicForCurrentLevel(t_CurrentLevel);
        if (t_AudioClipForGameplay != null && !AudioManager.Instance.IsSoundAlreadyPlayingInAnyAudioSource(t_AudioClipForGameplay))
        {

            audioManager.PlaySound(t_AudioClipForGameplay, true);
        }
    }

    public void StopBackgroundMusicForGameplay()
    {

        audioManager.StopSound(m_CurrentAudioClipForLevel);
    }

    public void PlaySoundFXForButtonSelect()
    {
        audioManager.PlaySound(buttonSelect, false);

    }

    public void PlaySoundFXForCoinEarn()
    {

        audioManager.PlaySound(coinEarn, false);
    }

    #endregion

}
