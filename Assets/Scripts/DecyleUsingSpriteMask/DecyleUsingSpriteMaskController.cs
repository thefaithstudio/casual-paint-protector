﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Gameplay;

[RequireComponent(typeof(PreLoadedPrefab))]
public class DecyleUsingSpriteMaskController : MonoBehaviour
{

    public static DecyleUsingSpriteMaskController Instance;

    #region Public Variables

    [Space(5.0f)]
    public Gradient defaultSplatGradient;
    [Range(0f, 1f)]
    public float    rotationVarient;
    [Range(0f,0.33f)]
    public float    scaleVarient;
    [Range(1f,5f)]
    public float    splatSize = 1f;
    
    [Space(5.0f)]
    public PreLoadedPrefab preloadedSpriteSplat;

    [Space(5.0f)]
    public SpriteRenderer contentSpriteRenderer;
    public SpriteRenderer backgroundSpriteRenderer;
    
    #endregion

    #region Private Variables

    private bool m_IsSpriteSplattingEnabled;

    private Gradient m_SplatGradient;

    private List<SpriteSplatter> m_ActiveSplatter;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        if (Instance == null)
        {

            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else {

            Destroy(gameObject);
        }
    }

    #endregion

    #region Configuretion

    private void CreateSplatter(Vector3 t_SplatPosition, Color t_SplatColor) {

        if (IsSpriteSplattingEnabled())
        {

            HapticFeedbackController.Instance.TapPeekVibrate();

            Quaternion t_RotationVarient = new Quaternion(0, 0, 1, Random.Range(0, rotationVarient) * 360);
            Vector3 t_ScaleVarient      = Vector3.one * splatSize * Random.Range(1f - scaleVarient, 1);

            GameObject t_NewSplatter = preloadedSpriteSplat.PullPreloadedPrefab();
            t_NewSplatter.transform.position = t_SplatPosition;
            t_NewSplatter.transform.rotation = t_RotationVarient;
            t_NewSplatter.transform.localScale = t_ScaleVarient;

            SpriteSplatter t_NewSpriteSplatter = t_NewSplatter.GetComponent<SpriteSplatter>();
            t_NewSpriteSplatter.ShowSplat(t_SplatColor);
            m_ActiveSplatter.Add(t_NewSpriteSplatter);
        }
    }

    private IEnumerator ControllerForHidingAllSplater() {

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        int t_NumberOfActiveSplatter = m_ActiveSplatter.Count;
        for (int i = 0; i < t_NumberOfActiveSplatter; i++) {

            m_ActiveSplatter[i].HideSplat();
            yield return t_CycleDelay;
        }

        StopCoroutine(ControllerForHidingAllSplater());
    }

    #endregion

    #region Public Callback

    public bool IsSpriteSplattingEnabled()
    {
        return m_IsSpriteSplattingEnabled;
    }

    public void EnableSpriteSplatting(Sprite contentSprite, Sprite backgroundSprite = null) {

        contentSpriteRenderer.sprite    = contentSprite;
        backgroundSpriteRenderer.sprite = backgroundSprite == null ? backgroundSpriteRenderer.sprite : backgroundSprite;

        m_ActiveSplatter                = new List<SpriteSplatter>();

        m_IsSpriteSplattingEnabled = true;
    }

    public void DisableSpriteSplatting() {

        m_IsSpriteSplattingEnabled = false;
    }

    public void CreateSplatterWithColor(Vector3 t_SplatPosition, Color t_SplatColor) {

        CreateSplatter(t_SplatPosition, t_SplatColor);
    }

    public void CreateSplatterWtthGradient(Vector3 t_SplatPosition, Gradient t_SplatGradeint = null) {

        if (t_SplatGradeint == null)
            m_SplatGradient = defaultSplatGradient;
        else
            m_SplatGradient = t_SplatGradeint;

        CreateSplatter(t_SplatPosition, m_SplatGradient.Evaluate(Random.Range(0f, 1f)));
    }

    public void HideAllSplat() {

        StartCoroutine(ControllerForHidingAllSplater());
    }

    #endregion
}
