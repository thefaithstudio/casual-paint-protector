﻿using UnityEngine;
using UnityEngine.Events;

public class UIStateController : MonoBehaviour
{
    #region Public Variables

    public static UIStateController Instance;

    public UIMainMenuPanelController UIMainMenuPanelControllerReference;
    public UIGameplayPanelController UIGameplayControllerReference;
    public UIGameOverPanelController UIGameOverPanelControllerReference;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        if (Instance == null)
        {

            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else {

            Destroy(gameObject);
        }
    }

    #endregion

    #region Public Callback :   MainMenu Panel

    public void AppearMainMenuPanel(int t_CurrentLevel = -1)
    {
        UIMainMenuPanelControllerReference.UpdateUIForTotalCoinEarned();
        UIMainMenuPanelControllerReference.AppearMainMenuPanel();

        if (t_CurrentLevel > -1)
            UpdateLevelInfo(t_CurrentLevel);
    }

    public void DisappearMainMenuPanel()
    {

        UIMainMenuPanelControllerReference.DisappearMainMenuPanel();
    }

    public void UpdateLevelInfo(int t_CurrentLevel) {

        UIMainMenuPanelControllerReference.UpdateLevelInfo(t_CurrentLevel);
    }

    public void UpdateUIForTotalCoinEarned() {

        UIMainMenuPanelControllerReference.UpdateUIForTotalCoinEarned();
    }

    #endregion

    #region Public Callback :   GamePlay Panel

    public void AppearGameplayPanel(bool t_IsBonusLevel, int t_NumberOfPlayer = 0, UnityAction t_OnPlayerUIDeployed = null) {

        UIGameplayControllerReference.AppearGameplayPanel(t_IsBonusLevel, t_NumberOfPlayer, t_OnPlayerUIDeployed);
    }

    public void DisappearGameplayPanel(bool t_IsBonusLevel) {

        UIGameplayControllerReference.DisappearGameplayPanel(t_IsBonusLevel);
    }

    public void ChangeRoundTextInfo(string t_RoundInfo) {

        UIGameplayControllerReference.ChangeRoundTextInfo(t_RoundInfo);
    }

    public void UpdateUIForCoinEarn(double t_TotalNumberOfCoinEarned) {

        UIGameplayControllerReference.UpdateUIForCoinEarn(t_TotalNumberOfCoinEarned);
    }

    public void UpdateUIForNumberOfFlight(int t_NumberOfFlightDeparted, int t_NumberOfFlightToBeDeparted) {

        UIGameplayControllerReference.UpdateUIForNumberOfFlight(t_NumberOfFlightDeparted, t_NumberOfFlightToBeDeparted);
    }

    public void UpdateUIForCargoStatusProgressBar(float t_Filler) {

        UIGameplayControllerReference.UpdateUIForTimer(t_Filler);
    }

    public void UpdatePlayerRankList(int t_PlayerIndex, int t_PointToBeAdd) {

        UIGameplayControllerReference.UpdatePlayerRankList(t_PlayerIndex, t_PointToBeAdd);
    }

    public int GetPlayerRankInRankList(string t_PlayerName) {

        return UIGameplayControllerReference.GetPlayerRankInRankList(t_PlayerName);
    }

    public void UpdateUIForWaterTankProgressBar(float t_Filler) {

        UIGameplayControllerReference.UpdateUIForWaterTankProgressBar(t_Filler);
    }

    public Sprite GetPlayerProfilePicture(int t_PlayerIndex) {

        return UIGameplayControllerReference.GetPlayerProfilePicture(t_PlayerIndex);
    }

    public PlayerInfo[] GetPlayerInfos() {

        return UIGameplayControllerReference.GetPlayerInfos();
    }

    public string GetPlayerName() {

        return UIMainMenuPanelControllerReference.GetCurrentPlayerName();
    }

    public void ShowShootingDialougeForPerfectShoot()
    {

        UIGameplayControllerReference.ShowShootingDialougeForPerfectShoot();
    }

    public void ShowShootingDialougeForAwesomeShoot()
    {

        UIGameplayControllerReference.ShowShootingDialougeForAwesomeShoot();
    }

    public void ShowShootingDialougeForGoodShoot()
    {

        UIGameplayControllerReference.ShowShootingDialougeForGoodShoot();
    }

    public void ShowShootingDialougeForFailedShoot()
    {

        UIGameplayControllerReference.ShowShootingDialougeForFailedShoot();
    }

    public void ShowLevelCompleteDialougeForSuccess()
    {

        UIGameplayControllerReference.ShowLevelCompleteDialougeForSuccess();
    }

    public void ShowLevelCompleteDialougeForFail()
    {

        UIGameplayControllerReference.ShowLevelCompleteDialougeForFail();
    }

    #endregion

    #region Public Callback     :   Game Over Panel

    public void AppearGameOverPanel(bool t_LevelCompleted, double t_TotalEarningOnCompletedLevel, UnityAction t_OnContinueButtonPressed) {

        UIGameOverPanelControllerReference.AppearGameOverPanel(t_LevelCompleted, t_TotalEarningOnCompletedLevel, t_OnContinueButtonPressed);
    }

    #endregion

}
