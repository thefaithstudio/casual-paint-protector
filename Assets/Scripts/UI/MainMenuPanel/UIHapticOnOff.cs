﻿using UnityEngine;
using UnityEngine.UI;

using com.faithstudio.GameplayService;

public class UIHapticOnOff : MonoBehaviour
{
    #region Public Variables

    public Sprite spriteForHapticEnabled;
    public Sprite spriteForHapticDisabled;

    public Animator buttonAnimator;
    public Image imageReference;
    public Button buttonReference;

    #endregion

    #region Private Variables

    private bool m_IsHapticFeedbackDisabled;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        buttonReference.onClick.AddListener(delegate
        {
            AudioController.Instance.PlaySoundFXForButtonSelect();
            buttonAnimator.SetTrigger("PRESSED");
            ToggleSoundEnabledAndDisabled();
        });
    }

    private void Start()
    {
        if (!HapticFeedbackController.Instance.IsHapticFeedbackEnabled())
        {
            m_IsHapticFeedbackDisabled = true;
        }
        else
            m_IsHapticFeedbackDisabled = false;

        MakeVisualChangeOfHapticFeedbackStatus();
    }

    #endregion

    #region Configuretion

    private void MakeVisualChangeOfHapticFeedbackStatus()
    {

        if (m_IsHapticFeedbackDisabled)
        {
            imageReference.sprite = spriteForHapticDisabled;
        }
        else
        {
            imageReference.sprite = spriteForHapticEnabled;
        }
    }

    #endregion

    #region Public Callback

    public void ToggleSoundEnabledAndDisabled()
    {

        m_IsHapticFeedbackDisabled = !m_IsHapticFeedbackDisabled;
        MakeVisualChangeOfHapticFeedbackStatus();
        if (m_IsHapticFeedbackDisabled)
        {

            HapticFeedbackController.Instance.DisableHapticFeedback();
        }
        else
        {
            HapticFeedbackController.Instance.EnableHapticFeedback();

        }
    }

    #endregion
}
