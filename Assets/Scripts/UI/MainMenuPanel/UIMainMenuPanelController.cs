﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

using com.faithstudio.UI;
using com.faithstudio.Math;

public class UIMainMenuPanelController : MonoBehaviour
{

    #region Public Variables

    [Header("Reference  :   External")]
    public GameplayController gameplayControllerReference;
    public UIUpgradeInfoController UIUpgradeControllerReference;
    public UILevelProgressionController UILevelProgressionControllerReference;
    public UISettingsController UISettingsControllerReference;

    [Space(5.0f)]
    [Header("-----------------")]
    public Animator mainMenuAnimator;
    public Animator mapButtonAnimator;

    [Space(5.0f)]
    public Button startButton;
    public Button mapButton;

    [Space(5.0f)]
    public TextMeshProUGUI coinInfoText;

    [Space(5.0f)]
    [Range(1,10)]
    public int characterLimitForname = 5;
    public TMP_InputField nameField;

    [Space(5.0f)]
    [Header("-----------------")]
    [Header("Section    :   Map Info")]
    public GeoLocationManager geoLocationManager;
    public Image mapIconImage;
    public TextMeshProUGUI levelInfoText;

    #endregion

    #region Private Variables

    private string m_PlayerName;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        startButton.onClick.AddListener(delegate
        {
            AudioController.Instance.PlaySoundFXForButtonSelect();
            gameplayControllerReference.PreProcess();
        });


        mapButton.onClick.AddListener(delegate
        {
            AudioController.Instance.PlaySoundFXForButtonSelect();
            mapButtonAnimator.SetTrigger("PRESSED");
            UILevelProgressionControllerReference.AppearLevelProgressionPanel();
        });

        nameField.characterLimit = characterLimitForname;
        nameField.onValueChanged.AddListener(delegate
        {
            m_PlayerName = nameField.text;
        });
    }

    private void Start()
    {
        
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForDisappear() {

        //AudioController.Instance.StopBackgroundMusicForMainMenu();

        PlayerStateController.Instance.SetPlayerName(m_PlayerName);

        mainMenuAnimator.SetTrigger("DISAPPEAR");
        mapButtonAnimator.SetTrigger("DISAPPEAR");
        UIUpgradeControllerReference.DisappearUpgradeInfoContainer();
        UISettingsControllerReference.DisappearSettingsButton();
        UISettingsControllerReference.HideSettings();

        yield return new WaitForSeconds(1f);

        StopCoroutine(ControllerForDisappear());

        if (gameObject.activeInHierarchy)
            gameObject.SetActive(false);
    }

    #endregion

    #region Public Callback

    public void AppearMainMenuPanel() {

        if (!gameObject.activeInHierarchy)
            gameObject.SetActive(true);

        m_PlayerName = PlayerStateController.Instance.GetPlayerName();

        if (m_PlayerName == "")
            m_PlayerName = "Dash!";
        else
            nameField.text = m_PlayerName;

        AudioController.Instance.PlayBackgroundMusicForMainMenu();

        mainMenuAnimator.SetTrigger("APPEAR");
        mapButtonAnimator.SetTrigger("APPEAR");
        UISettingsControllerReference.AppearSettingsButton();
        UIUpgradeControllerReference.AppearUpgradeInfoContainer();
    }

    public void DisappearMainMenuPanel() {

        StartCoroutine(ControllerForDisappear());
    }

    public void UpdateLevelInfo(int t_CurrentLevel) {

        mapIconImage.sprite     = geoLocationManager.GetMapIconForCurrentLevel(t_CurrentLevel);
        Vector2Int t_LevelRange = geoLocationManager.GetLevelRangeForCurrentLevel(t_CurrentLevel);
        
        levelInfoText.text = ((t_CurrentLevel + 2) - t_LevelRange.x) + "/" + ((t_LevelRange.y + 1) - t_LevelRange.x);
    }

    public void UpdateUIForTotalCoinEarned() {

        coinInfoText.text = MathFunction.Instance.GetCurrencyInFormat(GameManager.Instance.GetInGameCurrency());
    }

    public string GetCurrentPlayerName() {

        return m_PlayerName;
    }

    #endregion

}
