﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UILevelProgressionController : MonoBehaviour
{
    #region Custom Variables

    [System.Serializable]
    public struct MapInfoContainer
    {
        public TextMeshProUGUI mapTitleText;
        public TextMeshProUGUI mapInfoText;
        public Image mapIcon;
        public Animator         mapInfoContainerAnimator;
    }

    #endregion

    #region Public Variables

    [Header("Reference  :   External")]
    public LevelManager levelManagerReference;
    public GeoLocationManager geoLocationManagerReference;

    [Space(5.0f)]
    public Animator mapProgressionPanelAnimator;

    [Space(5.0f)]
    public Button backButton;
    public MapInfoContainer[] mapInfoContainer;

    #endregion

    #region Private Variables

    private int m_NumberOfLevelInfoContainer;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        m_NumberOfLevelInfoContainer = mapInfoContainer.Length;

        backButton.onClick.AddListener(delegate
        {
            AudioController.Instance.PlaySoundFXForButtonSelect();
            DisappearLevelProgressionPanel();
            UIStateController.Instance.AppearMainMenuPanel(levelManagerReference.GetCurrentLevel());
        });
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForLevelProgressionPanelAppear() {

        int t_CurrentLevel = levelManagerReference.GetCurrentLevel() + 1;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(0.1f);

        mapProgressionPanelAnimator.SetTrigger("APPEAR");

        Vector2Int t_LevelRange;

        for (int counter = 0; counter < m_NumberOfLevelInfoContainer; counter++) {

            t_LevelRange = geoLocationManagerReference.GetLevelRange(counter);

            mapInfoContainer[counter].mapIcon.sprite = geoLocationManagerReference.GetMapIcon(counter);

            if (t_CurrentLevel < t_LevelRange.x) {

                mapInfoContainer[counter].mapTitleText.text = "LOCKED";
                mapInfoContainer[counter].mapInfoText.text      = "Unlock at " + (t_LevelRange.x - 1);
                mapInfoContainer[counter].mapInfoContainerAnimator.SetTrigger("APPEAR");
            }
            else if (t_CurrentLevel >= t_LevelRange.x && t_CurrentLevel < t_LevelRange.y)
            {
                mapInfoContainer[counter].mapTitleText.text = geoLocationManagerReference.GetMapName(counter);
                mapInfoContainer[counter].mapInfoText.text = t_CurrentLevel + " / " + t_LevelRange.y;
                mapInfoContainer[counter].mapInfoContainerAnimator.SetTrigger("APPEAR_UNLOCKED");
            }
            else if (t_CurrentLevel >= t_LevelRange.y) {

                mapInfoContainer[counter].mapTitleText.text = geoLocationManagerReference.GetMapName(counter);
                mapInfoContainer[counter].mapInfoText.text = t_LevelRange.y + " / " + t_LevelRange.y;
                mapInfoContainer[counter].mapInfoContainerAnimator.SetTrigger("APPEAR_COMPLETE");
            }

            yield return t_CycleDelay;

        }

        StopCoroutine(ControllerForLevelProgressionPanelAppear());
    }

    private IEnumerator ControllerForLevelProgressionPanelDisappear()
    {

        WaitForSeconds t_CycleDelay = new WaitForSeconds(0.1f);

        for (int counter = m_NumberOfLevelInfoContainer - 1; counter >= 0; counter--)
        {

            mapInfoContainer[counter].mapInfoContainerAnimator.SetTrigger("DISAPPEAR");

            yield return t_CycleDelay;

        }

        mapProgressionPanelAnimator.SetTrigger("DISAPPEAR");

        StopCoroutine(ControllerForLevelProgressionPanelDisappear());
    }

    #endregion

    #region Public Callback

    public void AppearLevelProgressionPanel() {

        StartCoroutine(ControllerForLevelProgressionPanelAppear());
    }

    public void DisappearLevelProgressionPanel() {

        StartCoroutine(ControllerForLevelProgressionPanelDisappear());
    }

    #endregion
}
