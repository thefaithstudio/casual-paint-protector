﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using com.faithstudio.Math;

public class UIUpgradeInfoController : MonoBehaviour
{
    #region Public Variables

    [Header("Reference  : External")]
    public PlayerStateController playerStateController;

    [Space(5.0f)]
    [Header("Section    :   HydraGun")]
    public Animator animatorForHydraGun;
    public TextMeshProUGUI levelOfHydraGun;
    public TextMeshProUGUI currentStateOfHydraGun;
    public TextMeshProUGUI upgradeCostOfHydraGun;
    public Image progressBarForHydraGun;
    public Button buttonForHydraGun;

    [Space(5.0f)]
    [Header("Section    :   WaterPower")]
    public Animator animatorForWaterPower;
    public TextMeshProUGUI levelOfWaterPower;
    public TextMeshProUGUI currentStateOfWaterPower;
    public TextMeshProUGUI upgradeCostOfWaterPower;
    public Image progressBarForWaterPower;
    public Button buttonForWaterPower;

    [Space(5.0f)]
    [Header("Section    :   CoinEarn")]
    public Animator animatorForCoinEarn;
    public TextMeshProUGUI levelOfCoinEarn;
    public TextMeshProUGUI currentStateOfCoinEarn;
    public TextMeshProUGUI upgradeCostOfCoinEarn;
    public Image progressBarForCoinEarn;
    public Button buttonForCoinEarn;


    #endregion

    #region MonoBehaviour

    private void Awake()
    {
        buttonForHydraGun.onClick.AddListener(delegate
        {
            AudioController.Instance.PlaySoundFXForButtonSelect();
            playerStateController.UpgradeJetPack(delegate
            {
                UpdateUIForShieldSize();
                UIStateController.Instance.UpdateUIForTotalCoinEarned();
            });
        });

        buttonForWaterPower.onClick.AddListener(delegate
        {
            AudioController.Instance.PlaySoundFXForButtonSelect();
            playerStateController.UpgradeBulletSpeed(delegate
            {
                UpdateUIForBulletSpeed();
                UIStateController.Instance.UpdateUIForTotalCoinEarned();
            });
        });

        buttonForCoinEarn.onClick.AddListener(delegate
        {
            AudioController.Instance.PlaySoundFXForButtonSelect();
            playerStateController.UpgradeCoinEarn(delegate
            {
                UpdateUIForCoinEarn();
                UIStateController.Instance.UpdateUIForTotalCoinEarned();
            });
        });
    }

    private void Start()
    {
        UpdateUIForShieldSize();
        UpdateUIForBulletSpeed();
        UpdateUIForCoinEarn();

        AppearUpgradeInfoContainer();
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForAppear()
    {

        WaitForSeconds t_CycleDelay = new WaitForSeconds(0.1f);

        animatorForHydraGun.SetTrigger("APPEAR");

        yield return t_CycleDelay;

        animatorForWaterPower.SetTrigger("APPEAR");

        yield return t_CycleDelay;

        animatorForCoinEarn.SetTrigger("APPEAR");

        StopCoroutine(ControllerForAppear());
    }

    private IEnumerator ControllerForDisappear()
    {

        WaitForSeconds t_CycleDelay = new WaitForSeconds(0.1f);

        animatorForHydraGun.SetTrigger("DISAPPEAR");

        yield return t_CycleDelay;

        animatorForWaterPower.SetTrigger("DISAPPEAR");

        yield return t_CycleDelay;

        animatorForCoinEarn.SetTrigger("DISAPPEAR");

        StopCoroutine(ControllerForDisappear());
    }

    private void UpdateUIForShieldSize()
    {

        int t_CurrentLevel = playerStateController.GetCurrentLevelOfJetPack();
        levelOfHydraGun.text = "Lvl " + ((t_CurrentLevel >= playerStateController.GetMaxLevelOfJetPack()) ? "Max" : (t_CurrentLevel + 1).ToString());
        currentStateOfHydraGun.text = "+" + (100 + (t_CurrentLevel * 10)) + "%";
        upgradeCostOfHydraGun.text = MathFunction.Instance.GetCurrencyInFormat(playerStateController.GetUpgradeCostOfJetPack()) + "$";
        progressBarForHydraGun.fillAmount = playerStateController.GetUpgradeProgressionOfJetPack();
    }

    private void UpdateUIForBulletSpeed()
    {

        int t_CurrentLevel = playerStateController.GetCurrentLevelOfBulletSpeed();
        levelOfWaterPower.text = "Lvl " + ((t_CurrentLevel >= playerStateController.GetMaxLevelOfBulletSpeed()) ? "Max" : (t_CurrentLevel + 1).ToString());
        currentStateOfWaterPower.text = "+" + (100 + (t_CurrentLevel * 10)) + "%";
        upgradeCostOfWaterPower.text = MathFunction.Instance.GetCurrencyInFormat(playerStateController.GetUpgradeCostOfBulletSpeed()) + "$";
        progressBarForWaterPower.fillAmount = playerStateController.GetUpgradeProgressionOfBulletSpeed();
    }

    private void UpdateUIForCoinEarn()
    {

        int t_CurrentLevel = playerStateController.GetCurrentLevelOfCoinEarn();
        levelOfCoinEarn.text = "Lvl " + ((t_CurrentLevel >= playerStateController.GetMaxLevelOfCoinEarn()) ? "Max" : (t_CurrentLevel + 1).ToString());
        currentStateOfCoinEarn.text = "+" + (100 + (t_CurrentLevel * 10)) + "%";
        upgradeCostOfCoinEarn.text = MathFunction.Instance.GetCurrencyInFormat(playerStateController.GetUpgradeCostOfCoinEarn()) + "$";
        progressBarForCoinEarn.fillAmount = playerStateController.GetUpgradeProgressionOfCoinEarn();
    }

    #endregion

    #region Public Callback

    public void AppearUpgradeInfoContainer()
    {

        StartCoroutine(ControllerForAppear());
    }

    public void DisappearUpgradeInfoContainer()
    {

        StartCoroutine(ControllerForDisappear());
    }

    #endregion
}
