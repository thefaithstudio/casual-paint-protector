﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

using com.faithstudio.Math;

[System.Serializable]
public class PlayerInfo
{

    public int playerRank;
    public int playerScore;
    public string playerName;
    public Sprite playerPicture;

    public PlayerInfo(int playerRank, int playerScore, string playerName, Sprite playerPicture)
    {
        this.playerRank = playerRank;
        this.playerScore = playerScore;
        this.playerName = playerName;
        this.playerPicture = playerPicture;
    }

}

public class UIGameplayPanelController : MonoBehaviour
{
    #region Custom Variables

    [System.Serializable]
    public class PlayerPosition
    {
        public Animator         animatorReference;

        [Space]
        public TextMeshProUGUI  playerNameText;
        public TextMeshProUGUI  playerScoreText;
        public Image            playerPictureImage;

        
    }

    #endregion

    #region Public Variables

    [Header("References :   Player & AI")]
    public Sprite[] AIProfilePictures;
    public string[] AINames;
    public PlayerPosition[] playerPositions;

    [Space(5.0f)]
    [Header("Animator   :   HUD")]
    public Animator coinInfoContainerAnimator;
    public Animator waterMeterInfoContainerAnimator;
    public Animator fireExtinguisherContainerAnimator;
    public Animator timerInfoContainerAnimator;

    [Space(5.0f)]
    [Header("Animator   :   Shooting Dialouge")]
    public Animator perfectShootingDialougeAnimator;
    public Animator awesomeShootingDialougeAnimator;
    public Animator goodShootingDialougeAnimator;
    public Animator failedShootingDialougeAnimator;

    [Space(5.0f)]
    [Header("Animator   :   Level Complete Dialouge")]
    public Animator levelCompleteWithSuccessDialougeAnimator;
    public Animator levelCompleteWithFailDialougeAnimator;

    [Space(5.0f)]
    [Header("Section    :   Info")]
    public Image progressBarForWaterTank;
    public Image progressBarForTimer;
    public TextMeshProUGUI  numberOfFireToDemolishedText;
    public TextMeshProUGUI  roundTextInfo;
    public TextMeshProUGUI  coinEarnedText;


    #endregion

    #region Private Variables

    private bool m_IsGameplayPanelShowing;

    private int m_NumberOfPlayer;

    private PlayerInfo[] m_PlayerInfos;

    #endregion

    
    #region Configuretion

    private IEnumerator ControllerForAppear(bool t_IsBonusLevel, UnityAction t_OnPlayerUIDeployed) {

        WaitForSeconds t_CycleDelay = new WaitForSeconds(0.25f);

        coinEarnedText.text = "0$";
        coinInfoContainerAnimator.SetTrigger("APPEAR");

        if (!t_IsBonusLevel) {

            yield return t_CycleDelay;

            timerInfoContainerAnimator.SetTrigger("APPEAR");
        }

        //Setting Value
        
        int             t_NumberOfTry;
        string          t_SelectedName = null;
        List<string>    t_NameAlreadyAssigned = new List<string>();
        Sprite          t_SelectedSprite = null;
        List<Sprite>    t_ProfilePictureAlreadyAssigned = new List<Sprite>();
        for (int counter = 0; counter < m_NumberOfPlayer; counter++)
        {
            if (counter == 0)
            {
                t_SelectedName = PlayerStateController.Instance.GetPlayerName();
                t_SelectedSprite = PlayerStateController.Instance.playerProfilePicture;
            }
            else {

                t_NumberOfTry = 0;
                while (t_NumberOfTry < 10)
                {

                    t_SelectedName = AINames[Random.Range(0, AINames.Length)];

                    if (!t_NameAlreadyAssigned.Contains(t_SelectedName))
                    {
                        t_NameAlreadyAssigned.Add(t_SelectedName);
                        break;
                    }
                    t_NumberOfTry++;
                }

                t_NumberOfTry = 0;
                while (t_NumberOfTry < 10)
                {
                    t_SelectedSprite = AIProfilePictures[Random.Range(0, AIProfilePictures.Length)];

                    if (!t_ProfilePictureAlreadyAssigned.Contains(t_SelectedSprite))
                    {
                        t_ProfilePictureAlreadyAssigned.Add(t_SelectedSprite);
                        break;
                    }
                    t_NumberOfTry++;
                }
            }

            m_PlayerInfos[counter] = new PlayerInfo (
                counter,
                0,
                t_SelectedName,
                t_SelectedSprite
            );

            playerPositions[counter].playerNameText.text = t_SelectedName;
            playerPositions[counter].playerScoreText.text = "0pts";
            playerPositions[counter].playerPictureImage.sprite = t_SelectedSprite;

            
        }

        for (int counter = 0; counter < m_NumberOfPlayer; counter++) {

            yield return t_CycleDelay;

            playerPositions[counter].animatorReference.SetTrigger("APPEAR");
        }

        if (t_OnPlayerUIDeployed != null)
            t_OnPlayerUIDeployed.Invoke();

        timerInfoContainerAnimator.SetTrigger("INFO_UPDATE_LOOP");

        m_IsGameplayPanelShowing = true;

        StopCoroutine(ControllerForAppear(false, null));
    }

    private IEnumerator ControllerForDisappear(bool t_IsBonusLevel) {

        WaitForSeconds t_CycleDelay = new WaitForSeconds(0.25f);

        coinInfoContainerAnimator.SetTrigger("DISAPPEAR");

        if (!t_IsBonusLevel) {

            yield return t_CycleDelay;

            timerInfoContainerAnimator.SetTrigger("DISAPPEAR");
        }

        for (int counter = 0; counter < m_NumberOfPlayer; counter++)
        {
            yield return t_CycleDelay;
            playerPositions[counter].animatorReference.SetTrigger("DISAPPEAR");
        }

        yield return new WaitForSeconds(1);

        StopCoroutine(ControllerForDisappear(false));

        m_IsGameplayPanelShowing = false;

        if (gameObject.activeInHierarchy)
            gameObject.SetActive(false);
    }

    private bool IsValidPlayerPosition(int t_PlayerIndex) {

        if (t_PlayerIndex >= 0 && t_PlayerIndex < m_NumberOfPlayer)
            return true;

        Debug.LogError("Invalid PlayerIndex : " + t_PlayerIndex);

        return false;
    }

    #endregion

    #region Public Callback

    public void AppearGameplayPanel(bool t_IsBonusLevel, int t_NumberOfPlayer = 0, UnityAction t_OnPlayerUIDeployed = null) {

        if (!gameObject.activeInHierarchy)
            gameObject.SetActive(true);

        m_NumberOfPlayer= t_NumberOfPlayer;
        m_PlayerInfos   = new PlayerInfo[m_NumberOfPlayer];
        StartCoroutine(ControllerForAppear(t_IsBonusLevel, t_OnPlayerUIDeployed));
    }

    public void DisappearGameplayPanel(bool t_IsBonusLevel) {

        StartCoroutine(ControllerForDisappear(t_IsBonusLevel));
    }

    public void ChangeRoundTextInfo(string t_RoundInfo) {

        roundTextInfo.text = t_RoundInfo;
    }

    public void UpdateUIForCoinEarn(double t_TotalNumberOfCoinEarned)
    {
        coinEarnedText.text = MathFunction.Instance.GetCurrencyInFormat(t_TotalNumberOfCoinEarned);

        if(m_IsGameplayPanelShowing)
            coinInfoContainerAnimator.SetTrigger("INFO_UPDATE");
    }

    public void UpdateUIForNumberOfFlight(int t_NumberOfFlightDeparted, int t_NumberOfFlightToBeDeparted)
    {
        numberOfFireToDemolishedText.text = t_NumberOfFlightDeparted + "/" + t_NumberOfFlightToBeDeparted;

        if (m_IsGameplayPanelShowing)
            fireExtinguisherContainerAnimator.SetTrigger("INFO_UPDATE");
    }

    public void UpdateUIForTimer(float t_Filler){

        progressBarForTimer.fillAmount = t_Filler;

        //if (m_IsGameplayPanelShowing)
        //    timerInfoContainerAnimator.SetTrigger("INFO_UPDATE");
    }

    public void UpdateUIForWaterTankProgressBar(float t_Filler)
    {
        progressBarForWaterTank.fillAmount = t_Filler;
    }

    public void UpdatePlayerRankList(int t_PlayerIndex, int t_PointToBeAdd) {

        if (IsValidPlayerPosition(t_PlayerIndex)) {

            m_PlayerInfos[t_PlayerIndex].playerScore += t_PointToBeAdd;

            //Sorting Rank
            int t_TempRank = -1;
            for (int i = 0; i < m_NumberOfPlayer; i++) {

                for (int j = 0; j < m_NumberOfPlayer; j++) {

                    if (i != j
                     && m_PlayerInfos[i].playerScore < m_PlayerInfos[j].playerScore
                     && m_PlayerInfos[j].playerRank > m_PlayerInfos[i].playerRank) {

                        t_TempRank = m_PlayerInfos[i].playerRank;
                        m_PlayerInfos[i].playerRank = m_PlayerInfos[j].playerRank;
                        m_PlayerInfos[j].playerRank = t_TempRank;


                    }
                }
            }

            for (int i = 0; i < m_NumberOfPlayer; i++)
            {

                for (int j = 0; j < m_NumberOfPlayer; j++)
                {

                    if (i == m_PlayerInfos[j].playerRank)
                    {

                        playerPositions[i].playerNameText.text = m_PlayerInfos[j].playerName;
                        playerPositions[i].playerScoreText.text = m_PlayerInfos[j].playerScore + "pts";
                        playerPositions[i].playerPictureImage.sprite = m_PlayerInfos[j].playerPicture;

                    }
                }
            }

            if (t_TempRank != -1)
            {
                playerPositions[t_TempRank].animatorReference.SetTrigger("INFO_UPDATE");
            }
        }
    }

    public int GetPlayerRankInRankList(string t_PlayerName) {

        for (int i = 0; i < m_NumberOfPlayer; i++) {

            if (t_PlayerName == m_PlayerInfos[i].playerName) {

                return m_PlayerInfos[i].playerRank;
            }
        }

        return 0;
    }

    public Sprite GetPlayerProfilePicture(int t_PlayerIndex) {

        if (IsValidPlayerPosition(t_PlayerIndex)) {

            return m_PlayerInfos[t_PlayerIndex].playerPicture;
        }

        Debug.LogError("Couldn't find any profile picture as the player index is invalid");

        return null;
    }

    public PlayerInfo[] GetPlayerInfos() {

        return m_PlayerInfos;
    }

    public void ShowShootingDialougeForPerfectShoot() {

        perfectShootingDialougeAnimator.SetTrigger("APPEAR");
    }

    public void ShowShootingDialougeForAwesomeShoot()
    {

        awesomeShootingDialougeAnimator.SetTrigger("APPEAR");
    }

    public void ShowShootingDialougeForGoodShoot()
    {

        goodShootingDialougeAnimator.SetTrigger("APPEAR");
    }

    public void ShowShootingDialougeForFailedShoot()
    {

        failedShootingDialougeAnimator.SetTrigger("APPEAR");
    }

    public void ShowLevelCompleteDialougeForSuccess()
    {

        levelCompleteWithSuccessDialougeAnimator.SetTrigger("APPEAR");
    }

    public void ShowLevelCompleteDialougeForFail()
    {

        levelCompleteWithFailDialougeAnimator.SetTrigger("APPEAR");
    }

    #endregion
}
