﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class UIColorGunInfo : MonoBehaviour
{
    #region Public Variables

    public Vector3          positionOffSet;

    [Space(10.0f)]
    public Animator         animatorReference;

    [Space(10.0f)]
    public TextMeshProUGUI  messageToDisplay;
    public Button           buttonForShootingColorGun;

    #endregion

    #region Private Variables
    
    private UnityAction OnInteractionButtonClick;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        buttonForShootingColorGun.onClick.AddListener(delegate
        {
            OnInteractionButtonClick?.Invoke();
        });
    }

    #endregion

    #region Configuretion

    #endregion

    #region Public Callback

    public void PreProcess(Vector3 t_WorldPosition, UnityAction OnInteractionButtonClick) {

        Vector3 t_ModifiedPosition = t_WorldPosition + positionOffSet;
        transform.position = t_ModifiedPosition;
        
        this.OnInteractionButtonClick   = OnInteractionButtonClick;
    }

    public void PostProcess() {

        OnInteractionButtonClick = null;
    }

    public void ShowMessage(string t_Message, float t_AnimationSpeed = 1f, Color ? t_TextColor = null, bool t_LoopingTheMessage = false) {

        t_TextColor = t_TextColor == null ? Color.white : t_TextColor;

        messageToDisplay.text   = t_Message;
        messageToDisplay.color  = (UnityEngine.Color)t_TextColor;
        

        if (!t_LoopingTheMessage)
        {
            animatorReference.speed = t_AnimationSpeed;
            animatorReference.SetTrigger("TIME_COUNT");
        }
        else
        {
            animatorReference.speed = 1f;
            animatorReference.SetTrigger("LOOP");
        } 
    }

    #endregion
}
