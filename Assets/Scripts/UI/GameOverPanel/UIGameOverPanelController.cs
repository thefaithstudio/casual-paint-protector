﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using TMPro;

//using com.faithstudio.SDK;
using com.faithstudio.GameplayService;
using com.faithstudio.Math;

public class UIGameOverPanelController : MonoBehaviour
{

    #region Custom Variables

    [System.Serializable]
    public class UIForPlayerInfo
    {
        public Image            imageReferenceForProfilePicture;
        public TextMeshProUGUI  textReferenceForPlayerName;
    }

    #endregion

    #region Public Variables

    public ParticleSystem coinEarnByAdsParticle;
    public ParticleSystem levelCompleteParticle;

    [Space(5.0f)]
    public Animator particleBackgroundAnimator;
    public Animator animatorReference;
    public Animator watchAdPanelAnimatorReference;
    public Animator continueButtonAnimatorReference;

    [Space(5.0f)]
    public TextMeshProUGUI coinEarnedText;
    public TextMeshProUGUI coinCanBeEarnedFromAdText;

    [Space(5.0f)]
    public Button watchAdButton;
    public Button continueButton;

    [Space(5.0f)]
    public Sprite emptyProfile;
    public UIForPlayerInfo[] UIForPlayerInfoReference;

    #endregion

    #region Private Variables

    private bool m_IsWatchAdPanelAppeard;

    private bool m_LevelCompleted;
    private double m_TotalEarningOnCompletedLevel;
    private UnityAction m_OnContinueButtonPressed;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        watchAdButton.onClick.AddListener(delegate
        {
            AudioController.Instance.PlaySoundFXForButtonSelect();
            OnWatchAdButtonPressed();
        });

        continueButton.onClick.AddListener(delegate
        {
            AudioController.Instance.PlaySoundFXForButtonSelect();
            DisappearGameOverPanel();
        });
    }

    #endregion

    #region Configuretion

    private void OnWatchAdButtonPressed()
    {
        if (UniversalAdsController.Instance.IsRewardedVideoAdReady()) {

            UniversalAdsController.Instance.ShowRewardVideoAd(delegate
            {
                //UNCHECK : After Deploy
                //FacebookAnalyticsManager.Instance.FBRewardedVideoAd("CoinBoost_3x");
                //FirebaseAnalyticsEventController.Instance.UpdateFirebaseEventForRewardVideoAdOnBoost("CoinBoost_3x");

                StartCoroutine(ControllerForReward());
            });
        }
    }

    private IEnumerator ControllerForAppearGameOverPanel() {

        if(m_LevelCompleted)
            levelCompleteParticle.Play();

        PlayerInfo[] t_PlayerInfo = UIStateController.Instance.GetPlayerInfos();
        int t_NumberOfPlayerPlayedTheGame = t_PlayerInfo.Length;
        int t_NumberOfSlotAvailableToDisplayPlayerInfo = UIForPlayerInfoReference.Length;
        for (int playerInfoIndex = 0; playerInfoIndex < t_NumberOfSlotAvailableToDisplayPlayerInfo; playerInfoIndex++) {

            if (playerInfoIndex < t_NumberOfPlayerPlayedTheGame) {

                for (int participatedPlayer = 0; participatedPlayer < t_NumberOfPlayerPlayedTheGame; participatedPlayer++) {

                    if (playerInfoIndex == t_PlayerInfo[participatedPlayer].playerRank) {

                        UIForPlayerInfoReference[playerInfoIndex].imageReferenceForProfilePicture.sprite    = t_PlayerInfo[participatedPlayer].playerPicture;
                        UIForPlayerInfoReference[playerInfoIndex].textReferenceForPlayerName.text           = t_PlayerInfo[participatedPlayer].playerName;
                        break;
                    }
                }
            }
            else
            {
                UIForPlayerInfoReference[playerInfoIndex].imageReferenceForProfilePicture.sprite = emptyProfile;
                UIForPlayerInfoReference[playerInfoIndex].textReferenceForPlayerName.text = "None!";
            }
        }

        coinEarnedText.text = MathFunction.Instance.GetCurrencyInFormat(m_TotalEarningOnCompletedLevel);
        animatorReference.SetTrigger("APPEAR");
        particleBackgroundAnimator.SetTrigger("APPEAR");

        if (m_LevelCompleted)
        {

            float t_RemainingTimeForWaitingForAds = 2f;
            float t_CycleLength = 0.0167f;
            WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

            while (t_RemainingTimeForWaitingForAds >= 0)
            {
                //Enable : After Monetization Deployed
                //if (UniversalAdsController.Instance.IsRewardedVideoAdReady())
                //{
                //    coinCanBeEarnedFromAdText.text = MathFunction.Instance.GetCurrencyInFormat(m_TotalEarningOnCompletedLevel * 3);
                //    watchAdPanelAnimatorReference.SetTrigger("APPEAR");
                //    m_IsWatchAdPanelAppeard = true;
                //    yield return new WaitForSeconds(2f);
                //    break;
                //}

                yield return t_CycleDelay;
                t_RemainingTimeForWaitingForAds -= t_CycleLength;
            }
        }
        else {

            yield return new WaitForSeconds(1f);
        }

        continueButtonAnimatorReference.SetTrigger("APPEAR");
        StopCoroutine(ControllerForAppearGameOverPanel());
    }

    private IEnumerator ControllerForReward() {

        watchAdPanelAnimatorReference.SetTrigger("DISAPPEAR");
        m_IsWatchAdPanelAppeard = false;

        coinEarnByAdsParticle.Play();

        GameManager.Instance.UpdateInGameCurrency(m_TotalEarningOnCompletedLevel * 2.0);
        coinEarnedText.text = MathFunction.Instance.GetCurrencyInFormat(m_TotalEarningOnCompletedLevel * 3);

        yield return new WaitForSeconds(3f);

        coinEarnByAdsParticle.Stop();

        StopCoroutine(ControllerForReward());
    }

    private IEnumerator ControllerForDisappearGameOverPanel() {

        if (levelCompleteParticle.isPlaying)
            levelCompleteParticle.Stop();

        if (coinEarnByAdsParticle.isPlaying)
            coinEarnByAdsParticle.Stop();

        WaitForSeconds t_TransitionDelay = new WaitForSeconds(0.25f);

        if (m_IsWatchAdPanelAppeard) {

            watchAdPanelAnimatorReference.SetTrigger("DISAPPEAR");
            yield return t_TransitionDelay;
        }

        continueButtonAnimatorReference.SetTrigger("DISAPPEAR");

        yield return t_TransitionDelay;

        particleBackgroundAnimator.SetTrigger("DISAPPEAR");
        animatorReference.SetTrigger("DISAPPEAR");

        yield return t_TransitionDelay;

        m_OnContinueButtonPressed.Invoke();

        yield return new WaitForSeconds(1f);

        StopCoroutine(ControllerForDisappearGameOverPanel());

        if (gameObject.activeInHierarchy)
            gameObject.SetActive(false);
    }

    #endregion

    #region Public Callback

    public void AppearGameOverPanel(bool t_LevelCompleted, double t_TotalEarningOnCompletedLevel, UnityAction t_OnContinueButtonPressed) {

        if (!gameObject.activeInHierarchy)
            gameObject.SetActive(true);

        m_LevelCompleted = t_LevelCompleted;
        m_TotalEarningOnCompletedLevel = t_TotalEarningOnCompletedLevel;
        m_OnContinueButtonPressed = t_OnContinueButtonPressed;


        m_IsWatchAdPanelAppeard = false;
        StartCoroutine(ControllerForAppearGameOverPanel());
    }

    public void DisappearGameOverPanel() {

        StartCoroutine(ControllerForDisappearGameOverPanel());
    }

    #endregion
}
