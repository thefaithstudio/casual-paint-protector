﻿using UnityEngine;

using System.Collections;
using com.faithstudio.Gameplay;

public class CharacterControllerForDefender : MonoBehaviour
{

    #region Custom Variables

    public enum MovementType
    {
        Vertical,
        Horizontal,
        Both
    }

    public enum MovementArea
    {
        Fixed,
        Free
    }

    #endregion

    #region Public Variables

    public MovementType movementType;
    public MovementArea movementArea;
    public float        movementAreaBoundary;

    //----------
    public ForceMode    forceModeForMovement;
    public float        angulerVelocityForMovement;
    public float        forwardVelocityForMovement;

    //----------
    public Rigidbody    playerRigidbodyReference;
    public Transform    playerMeshTransformReference;
    public Animator     playerAnimatorReference;

    #endregion

    #region Private Variables

    private bool        m_IsDefendingCharacterControllerRunning;
    private bool        m_IsTouchPressed;

    private int         m_CurrentAnimationIndex = 0;

    private Transform   m_TransformReference;

    private Vector3     m_InitialPosition;
    private Vector3     m_Direction;
    private Vector3     m_MovementDirection;

    private Vector3     m_PreviousTouchPosition;

    private Vector3     m_TouchDownPosition;
    private Vector3     m_TouchPosition;
    private Vector3     m_TouchUpPosition;

    private Quaternion  m_LookDirection;
    private Quaternion  m_ModifiedRotation;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        m_TransformReference    = transform;
        m_InitialPosition       = m_TransformReference.position;
    }

    private void Start()
    {
        EnableCharacterControllerForDefender();

    }


    #endregion

    #region Configuretion

    private IEnumerator ControllerForRestoringRotation()
    {
        float t_CycleLength = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        Quaternion t_RestoringRotation = Quaternion.Euler(0, 180, 0);
        Quaternion t_ModifiedRotation;

        while (!m_IsTouchPressed) {

            t_ModifiedRotation = Quaternion.Slerp(
                    playerMeshTransformReference.rotation,
                    t_RestoringRotation,
                    angulerVelocityForMovement
                );

            playerMeshTransformReference.rotation = t_ModifiedRotation;

            yield return t_CycleDelay;
        }

        StopCoroutine(ControllerForRestoringRotation());
    }

    private IEnumerator ControllerForCharacterMovement() {

        float t_CycleLength = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        while (m_IsTouchPressed) {

            if (movementArea == MovementArea.Fixed)
            {
                if (Vector3.Distance((m_TransformReference.position + m_MovementDirection), m_InitialPosition) <= movementAreaBoundary)
                {
                    MovePlayer();
                }
            }else if (movementArea == MovementArea.Free)
            {
                MovePlayerByForce();
            }

            RotatePlayer();

            yield return t_CycleDelay;
        }
    }

    private void MovePlayerByForce()
    {
        playerRigidbodyReference.AddForce(
                    m_MovementDirection * forwardVelocityForMovement,
                    forceModeForMovement
                );
    }

    private void MovePlayer() {

        m_TransformReference.Translate(m_MovementDirection * forwardVelocityForMovement * Time.deltaTime);
    }

    private void RotatePlayer() {

        if (m_Direction != Vector3.zero)
        {
            m_LookDirection = Quaternion.LookRotation(
                new Vector3(
                        m_Direction.x,
                        0,
                        m_Direction.y
                    )
            );
            m_ModifiedRotation = Quaternion.Slerp(
                    playerMeshTransformReference.rotation,
                    m_LookDirection,
                    angulerVelocityForMovement
                );
            playerMeshTransformReference.rotation = m_ModifiedRotation;
        }

    }

    private void SwitchToAnimation(string t_AnimationState) {

        if (t_AnimationState == "SHIELD_IDLE") {

            if (m_CurrentAnimationIndex != 0) {

                m_CurrentAnimationIndex = 0;
                playerAnimatorReference.SetTrigger("SHIELD_IDLE");
            }
        }else if (t_AnimationState == "SHIELD_STRAFE")
        {

            if (m_CurrentAnimationIndex != 1)
            {

                m_CurrentAnimationIndex = 1;
                playerAnimatorReference.SetTrigger("SHIELD_STRAFE");
            }
        }
    }

    #endregion

    #region Configuretion   :   Touch

    public void OnTouchDown(Vector3 t_TouchPosition)
    {

        m_TouchDownPosition     = t_TouchPosition;

        m_PreviousTouchPosition = m_TouchPosition;

        m_IsTouchPressed = true;
        StartCoroutine(ControllerForCharacterMovement());
    }

    public void OnTouch(Vector3 t_TouchPosition)
    {

        m_TouchPosition         = t_TouchPosition;

        m_Direction             = m_TouchPosition - m_PreviousTouchPosition;

        switch (movementType)
        {

            case MovementType.Vertical:
                m_MovementDirection = Vector3.Normalize(
                        new Vector3(
                                0,
                                0,
                                m_Direction.y
                            )
                    );
                break;
            case MovementType.Horizontal:
                m_MovementDirection = Vector3.Normalize(
                        new Vector3(
                                m_Direction.x,
                                0,
                                0
                            )
                    );
                break;
            case MovementType.Both:
                m_MovementDirection = Vector3.Normalize(
                        new Vector3(
                                m_Direction.x,
                                0,
                                m_Direction.y
                            )
                    );
                break;
        }


        SwitchToAnimation("SHIELD_STRAFE");
    }

    public void OnTouchUp(Vector3 t_TouchPosition)
    {

        SwitchToAnimation("SHIELD_IDLE");

        m_TouchUpPosition = t_TouchPosition;

        m_IsTouchPressed = false;
        StartCoroutine(ControllerForRestoringRotation());
    }

    #endregion

    #region Public Callback

    public bool IsDefendingCharacterControllerEnabled() {

        return m_IsDefendingCharacterControllerRunning;
    }

    public void EnableCharacterControllerForDefender(bool t_IsControlledByPlayer = false)
    {
        if (t_IsControlledByPlayer) {

            GlobalTouchController.Instance.OnTouchDown += OnTouchDown;
            GlobalTouchController.Instance.OnTouch += OnTouch;
            GlobalTouchController.Instance.OnTouchUp += OnTouchUp;
        }
        
        m_IsDefendingCharacterControllerRunning = true;
    }

    public void DisableCharacterControllerForDefender(bool t_IsControlledByPlayer = false) {

        if (t_IsControlledByPlayer) {

            GlobalTouchController.Instance.OnTouchDown -= OnTouchDown;
            GlobalTouchController.Instance.OnTouch -= OnTouch;
            GlobalTouchController.Instance.OnTouchUp -= OnTouchUp;
        }

        m_IsDefendingCharacterControllerRunning = false;
    }

    #endregion
}
