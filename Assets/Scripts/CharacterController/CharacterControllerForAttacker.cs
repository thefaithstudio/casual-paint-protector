﻿using System.Collections;
using UnityEngine;

public class CharacterControllerForAttacker : MonoBehaviour
{
    #region Public Variables

    public static CharacterControllerForAttacker Instance;

    public Animator playerAnimatorReference;

    [Space(5.0f)]
    public Transform initialPosition;
    public Transform meshTransformReference;

    [Space(5.0f)]
    [Range(0f, 1f)]
    public float angulerVelocityForMovement;
    [Range(0f, 10f)]
    public float forwardVelocityForMovement;

    #endregion

    #region Private Variables

    private ColorTurretAttribute m_ColorGunAttribute;

    private Transform m_TransformReference;
    private Transform m_TransformReferenceOfTarget;

    private Quaternion m_InitialRotation;

    private bool m_IsMovingTowardsTheTarget;

    private int m_CurrentAnimationIndex = 0;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {

        Instance = this;

        m_TransformReference = transform;
        m_InitialRotation = meshTransformReference.rotation;
    }

    #endregion

    #region Configuretion

    private void SwitchToAnimation(string t_AnimationState)
    {

        if (t_AnimationState == "IDLE")
        {

            if (m_CurrentAnimationIndex != 0)
            {

                m_CurrentAnimationIndex = 0;
                playerAnimatorReference.SetTrigger("IDLE");
            }
        }
        else if (t_AnimationState == "RUN")
        {

            if (m_CurrentAnimationIndex != 1)
            {

                m_CurrentAnimationIndex = 1;
                playerAnimatorReference.SetTrigger("RUN");
            }
        }
        else if (t_AnimationState == "PRESS")
        {
            m_CurrentAnimationIndex = 2;
            playerAnimatorReference.SetTrigger("PRESS");
        }
    }

    private void RotateCharacter(Quaternion t_TargetRotation)
    {

        Quaternion t_ModifiedRotation = Quaternion.Slerp(
                meshTransformReference.rotation,
                t_TargetRotation,
                angulerVelocityForMovement
            );
        meshTransformReference.rotation = t_ModifiedRotation;
    }

    private IEnumerator ControllerForRotateTowardsInitialRotation()
    {
        float t_CycleLength = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        while (Quaternion.Angle(meshTransformReference.rotation, m_InitialRotation) > 0.1f)
        {

            RotateCharacter(m_InitialRotation);

            yield return t_CycleDelay;
        }

        StopCoroutine(ControllerForRotateTowardsInitialRotation());
    }

    private IEnumerator ControllerForMovingTowardsTarget()
    {

        float t_CycleLength = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        Vector3 t_LookDirection;
        Vector3 t_ModifiedPosition;

        while (m_IsMovingTowardsTheTarget)
        {

            t_LookDirection = m_TransformReferenceOfTarget.position - m_TransformReference.position;
            if (t_LookDirection != Vector3.zero)
                RotateCharacter(Quaternion.LookRotation(t_LookDirection));

            if (Vector3.Distance(m_TransformReference.position, m_TransformReferenceOfTarget.position) <= 0.1f)
            {

                if (m_ColorGunAttribute.IsShootingColorBulletAllowed())
                {
                    SwitchToAnimation("IDLE");
                    yield return StartCoroutine(ControllerForRotateTowardsInitialRotation());
                    SwitchToAnimation("PRESS");
                    yield return new WaitForSeconds(0.125f);
                    m_ColorGunAttribute.ShootColor();
                }
                else
                {

                    SwitchToAnimation("IDLE");
                }
                break;
            }

            t_ModifiedPosition = Vector3.MoveTowards(
                    m_TransformReference.position,
                    m_TransformReferenceOfTarget.position,
                    forwardVelocityForMovement * Time.deltaTime
                );
            m_TransformReference.position = t_ModifiedPosition;

            yield return t_CycleDelay;
        }

        StopCoroutine(ControllerForMovingTowardsTarget());
        m_IsMovingTowardsTheTarget = false;
    }

    #endregion

    #region Public Callback

    public void PreProcess()
    {

        m_TransformReference.position = initialPosition.position;
        meshTransformReference.rotation = m_InitialRotation;
    }

    public void PostProcess()
    {


    }

    public void SetTargetForCharacter(ColorTurretAttribute t_ColorGunAttribute)
    {

        if (!m_IsMovingTowardsTheTarget)
        {
            m_ColorGunAttribute = t_ColorGunAttribute;
            m_TransformReferenceOfTarget = t_ColorGunAttribute.GetTransformOfOperationPoint();

            if (Vector3.Distance(m_TransformReference.position, m_TransformReferenceOfTarget.position) >= 0.1f)
                SwitchToAnimation("RUN");

            m_IsMovingTowardsTheTarget = true;
            StartCoroutine(ControllerForMovingTowardsTarget());
        }
    }


    public Vector3 GetCharacterPosition()
    {

        return m_TransformReference.position;
    }

    #endregion
}
