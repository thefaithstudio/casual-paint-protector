﻿using System.Collections;
using UnityEngine;
using com.faithstudio.Math;

public class CharacterControllerForDefence : MonoBehaviour
{
    #region Public Variables

    [Space(5.0f)]
    [Header("Configuretion  :   Control")]
    public LerpTouchController lerpTouchControllerReference;
    public Transform verticalMark;
    public Transform horizontalMark;
    [Range(0f,1f)]
    public float forwardVelocity;

    [Space(5.0f)]
    public Transform    playerMeshTransformReference;
    public Animator     playerAnimatorReference;

    #endregion

    #region Private Variables

    private bool    m_IsDefendingCharacterControllerRunning;

    private int     m_CurrentAnimationIndex = 0;

    private Transform m_TransformReference;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        m_TransformReference = transform;
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForAnimationState()
    {
        float t_CycleLength = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);


        Vector3 t_ModifiedTarget;
        Vector3 t_ModifiedPosition;

        while (m_IsDefendingCharacterControllerRunning) {

            yield return t_CycleDelay;

            t_ModifiedTarget = new Vector3(
                    horizontalMark.position.x,
                    verticalMark.position.y,
                    (horizontalMark.position.z + verticalMark.position.z) / 2f
                );
            t_ModifiedPosition = Vector3.MoveTowards(
                    m_TransformReference.position,
                    t_ModifiedTarget,
                    forwardVelocity * 100 * Time.deltaTime
                );
            m_TransformReference.position = t_ModifiedPosition;

        }

        StopCoroutine(ControllerForAnimationState());
    }

    private void SwitchToAnimation(string t_AnimationState)
    {

        if (t_AnimationState == "SHIELD_IDLE")
        {

            if (m_CurrentAnimationIndex != 0)
            {

                m_CurrentAnimationIndex = 0;
                playerAnimatorReference.SetTrigger("SHIELD_IDLE");
            }
        }
        else if (t_AnimationState == "SHIELD_STRAFE")
        {

            if (m_CurrentAnimationIndex != 1)
            {

                m_CurrentAnimationIndex = 1;
                playerAnimatorReference.SetTrigger("SHIELD_STRAFE");
            }
        }
    }

    #endregion

    #region Public Callback

    public void EnableCharacterControllerForDefender(bool t_IsControlledByPlayer = false)
    {
        lerpTouchControllerReference.EnableLerpTouchController(t_IsControlledByPlayer);

        m_IsDefendingCharacterControllerRunning = true;
        StartCoroutine(ControllerForAnimationState());
    }

    public void DisableCharacterControllerForDefender()
    {

        lerpTouchControllerReference.DisableLerpTouchController();
        m_IsDefendingCharacterControllerRunning = false;
    }

    #endregion
}
