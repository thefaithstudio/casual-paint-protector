﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CharacterControllerForDefender))]
public class CharacterControllerForDefenderEditor : Editor
{
    private CharacterControllerForDefender Reference;

    private SerializedProperty movementType;
    private SerializedProperty movementArea;

    private SerializedProperty forceModeForMovement;

    private SerializedProperty playerRigidbodyReference;
    private SerializedProperty playerMeshTransformReference;
    private SerializedProperty playerAnimatorReference;

    private void OnEnable()
    {
        Reference = (CharacterControllerForDefender)target;

        movementType = serializedObject.FindProperty("movementType");
        movementArea = serializedObject.FindProperty("movementArea");

        forceModeForMovement = serializedObject.FindProperty("forceModeForMovement");

        playerRigidbodyReference = serializedObject.FindProperty("playerRigidbodyReference");
        playerMeshTransformReference = serializedObject.FindProperty("playerMeshTransformReference");
        playerAnimatorReference = serializedObject.FindProperty("playerAnimatorReference");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        CustomGUI();

        serializedObject.ApplyModifiedProperties();
    }

    private void CustomGUI()
    {
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(movementType);
        EditorGUILayout.PropertyField(movementArea);

        if (Reference.movementArea == CharacterControllerForDefender.MovementArea.Fixed) {

            Reference.movementAreaBoundary = EditorGUILayout.Slider(
                    "Area Boundary",
                    Reference.movementAreaBoundary,
                    0,
                    25f
                );
        }

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Configuretion   :   Velocity", EditorStyles.boldLabel);
        if(Reference.movementArea == CharacterControllerForDefender.MovementArea.Free)
            EditorGUILayout.PropertyField(forceModeForMovement);
        Reference.angulerVelocityForMovement = EditorGUILayout.Slider(
                    "angulerVelocity",
                    Reference.angulerVelocityForMovement,
                    0,
                    1f
                );
        Reference.forwardVelocityForMovement = EditorGUILayout.Slider(
                    "forwardVeloity",
                    Reference.forwardVelocityForMovement,
                    0,
                    250f
                );

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Configuretion   :   ExternalReference", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(playerRigidbodyReference);
        EditorGUILayout.PropertyField(playerMeshTransformReference);
        EditorGUILayout.PropertyField(playerAnimatorReference);


    }
}
