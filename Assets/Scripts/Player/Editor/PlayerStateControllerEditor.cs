﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerStateController))]
public class PlayerStateControllerEditor : Editor
{
    private PlayerStateController Reference;

    private void OnEnable()
    {
        Reference = (PlayerStateController)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.Space();

        CustomGUI();
        
        EditorGUILayout.Space();

        DrawDefaultInspector();

        serializedObject.ApplyModifiedProperties();
    }

    private void CustomGUI() {

        
    }
}
