﻿
using UnityEngine;
using UnityEngine.Events;
using com.faithstudio.Gameplay;

public class PlayerStateController : MonoBehaviour
{

    #region Public Variables

    public static PlayerStateController Instance;

    public SkillTree playerSkillTree;

    [Space(5.0f)]
    public Sprite playerProfilePicture;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else {

            Destroy(gameObject);
        }
    }

    #endregion

    #region Public Callback :   Player General Info

    public string GetPlayerName() {

        return PlayerPrefs.GetString("PlayerData_PlayerName", "");
    }

    public void SetPlayerName(string t_NewPlayerName) {

        PlayerPrefs.SetString("PlayerData_PlayerName", t_NewPlayerName);
    }

    #endregion

    #region Public Callback :   SkillInfo


    public double GetUpgradeCostOfJetPack() {

        return playerSkillTree.GetUpgradeCostForNextLevel(0);
    }

    public double GetUpgradeCostOfBulletSpeed()
    {

        return playerSkillTree.GetUpgradeCostForNextLevel(1);
    }

    public double GetUpgradeCostOfCoinEarn()
    {

        return playerSkillTree.GetUpgradeCostForNextLevel(2);
    }

    public void UpgradeJetPack(UnityAction t_OnUpgradedSuccessfully = null)
    {
        double t_CostOfUpgrade = GetUpgradeCostOfJetPack();
        if (GameManager.Instance.GetInGameCurrency() >= t_CostOfUpgrade)
        {

            GameManager.Instance.DeductInGameCurrency(t_CostOfUpgrade);
            playerSkillTree.UpgradeSkill(0);

            if (t_OnUpgradedSuccessfully != null)
                t_OnUpgradedSuccessfully.Invoke();
        }
        else {

            Debug.LogWarning("Insufficient Fund");
        }
    }

    public void UpgradeBulletSpeed(UnityAction t_OnUpgradedSuccessfully = null)
    {
        double t_CostOfUpgrade = GetUpgradeCostOfBulletSpeed();
        if (GameManager.Instance.GetInGameCurrency() >= t_CostOfUpgrade)
        {

            GameManager.Instance.DeductInGameCurrency(t_CostOfUpgrade);
            playerSkillTree.UpgradeSkill(1);

            if (t_OnUpgradedSuccessfully != null)
                t_OnUpgradedSuccessfully.Invoke();
        }
        else
        {

            Debug.LogWarning("Insufficient Fund");
        }
    }

    public void UpgradeCoinEarn(UnityAction t_OnUpgradedSuccessfully = null)
    {
        double t_CostOfUpgrade = GetUpgradeCostOfCoinEarn();
        if (GameManager.Instance.GetInGameCurrency() >= t_CostOfUpgrade)
        {

            GameManager.Instance.DeductInGameCurrency(t_CostOfUpgrade);
            playerSkillTree.UpgradeSkill(2);

            if (t_OnUpgradedSuccessfully != null)
                t_OnUpgradedSuccessfully.Invoke();
        }
        else
        {

            Debug.LogWarning("Insufficient Fund");
        }
    }

    public int GetCurrentLevelOfJetPack() {

        return playerSkillTree.GetCurrentLevelOfSkill(0);
    }

    public float GetCurrentStateOfJetPack() {

        return playerSkillTree.GetCurrentStatOfSkill(0);
    }

    public int GetCurrentLevelOfBulletSpeed()
    {

        return playerSkillTree.GetCurrentLevelOfSkill(1);
    }

    public float GetCurrentStateOfBulletSpeed()
    {

        return playerSkillTree.GetCurrentStatOfSkill(1);
    }

    public int GetCurrentLevelOfCoinEarn()
    {

        return playerSkillTree.GetCurrentLevelOfSkill(2);
    }

    public float GetCurrentStateOfCoinEarn()
    {

        return playerSkillTree.GetCurrentStatOfSkill(2);
    }

    public float GetUpgradeProgressionOfJetPack() {

        return playerSkillTree.GetUpgradeProgression(0);
    }

    public float GetUpgradeProgressionOfBulletSpeed()
    {

        return playerSkillTree.GetUpgradeProgression(1);
    }

    public float GetUpgradeProgressionOfCoinEarn()
    {

        return playerSkillTree.GetUpgradeProgression(2);
    }

    public int GetMaxLevelOfJetPack() {

        return playerSkillTree.GetMaximumLevelOfSkill(0);
    }

    public int GetMaxLevelOfBulletSpeed()
    {

        return playerSkillTree.GetMaximumLevelOfSkill(1);
    }

    public int GetMaxLevelOfCoinEarn()
    {

        return playerSkillTree.GetMaximumLevelOfSkill(2);
    }

    #endregion
}
