﻿using UnityEngine;

public class TargetAttribute : MonoBehaviour
{
    #region Public Callback

    public SpriteRenderer targetSprite;

    #endregion

    #region Private Variables

    #endregion

    #region Mono Behaviour

    #endregion

    #region Configuretion

    #endregion

    #region Public Callback

    public void PreProcess(Color t_TargetColor) {

        targetSprite.color = t_TargetColor;
    }

    public void PostProcess() {

    }

    public void EnableSpriteRendererOfTarget()
    {
        targetSprite.enabled = true;
    }

    public void DisableSpriteRendererOfTarget()
    {
        targetSprite.enabled = false;    
    }

    #endregion
}
