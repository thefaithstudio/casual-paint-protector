﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using com.faithstudio.Gameplay;

public class ColorTurretController : MonoBehaviour
{
    #region Custom Variables

    private class TargetInfo
    {
        public Transform transformReference;
        public TargetAttribute colorGunTargetAttributeReference;

        public Vector3 targetedPosition;

        public TargetInfo(Transform transformReference, TargetAttribute colorGunTargetAttributeReference)
        {

            targetedPosition = transformReference.position;

            this.transformReference = transformReference;
            this.colorGunTargetAttributeReference = colorGunTargetAttributeReference;
        }
    }

    #endregion

    #region Public Variables

    [Header("Configuretion  :   Target")]
    public OnDemandPrefab targetPrefabContainer;
    public Transform paintTransformReference;
    public Vector3 offSetForPaintPosition;
    public Gradient gradientOfTarget;

    [Space(5.0f)]
    [Range(1f, 10f)]
    public float defaultTargetingVelocity;

    [Space(5.0f)]
    public ColorTurretAttribute[] colorGunAttributes;

    #endregion

    #region Private Variables

    private bool m_IsAutoTargetControllerRunning;

    private float m_TargetingVelocity;

    private List<int> m_ListOfActiveColorGunIndex;
    private List<TargetInfo> m_ListOfTargetInfos;

    #endregion

    #region Mono Behaviour

    #endregion

    #region Configuretion

    private void CreateATarget(int t_TargetIndex, Color t_TargetColor)
    {

        GameObject t_NewTarget = Instantiate(
                        targetPrefabContainer.GetObject(null),
                        paintTransformReference.position + offSetForPaintPosition,
                        Quaternion.identity
                    );

        m_ListOfTargetInfos.Add(new TargetInfo(
                t_NewTarget.transform,
                t_NewTarget.GetComponent<TargetAttribute>()
            ));

        m_ListOfTargetInfos[t_TargetIndex].colorGunTargetAttributeReference.PreProcess(
            t_TargetColor);
    }

    private IEnumerator ControllerForAutoTarget()
    {

        float t_CycleLength = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        float t_TextureWidth = paintTransformReference.localScale.x / 2.0f;
        float t_TextureHeight = paintTransformReference.localScale.y / 2.0f;

        int t_NumberOfTarget = m_ListOfTargetInfos.Count;

        Vector3 t_ModifiedPosition;
        float t_AbsoluteTargetingVelocity;

        while (m_IsAutoTargetControllerRunning)
        {

            t_AbsoluteTargetingVelocity = m_TargetingVelocity * Time.deltaTime;

            for (int targetIndex = 0; targetIndex < t_NumberOfTarget; targetIndex++)
            {

                if (Vector3.Distance(m_ListOfTargetInfos[targetIndex].transformReference.position, m_ListOfTargetInfos[targetIndex].targetedPosition) <= 0.1f)
                {

                    m_ListOfTargetInfos[targetIndex].targetedPosition = offSetForPaintPosition + new Vector3(
                            paintTransformReference.position.x + Random.Range(-t_TextureWidth, t_TextureWidth),
                            paintTransformReference.position.y + Random.Range(-t_TextureHeight, t_TextureHeight),
                            paintTransformReference.position.z
                        );
                }

                t_ModifiedPosition = Vector3.MoveTowards(
                    m_ListOfTargetInfos[targetIndex].transformReference.position,
                    m_ListOfTargetInfos[targetIndex].targetedPosition,
                    t_AbsoluteTargetingVelocity
                );

                m_ListOfTargetInfos[targetIndex].transformReference.position = t_ModifiedPosition;

                //Rotating  :   Target
                colorGunAttributes[m_ListOfActiveColorGunIndex[targetIndex]].RotateMuzzleHeadTowardsTarget(m_ListOfTargetInfos[targetIndex].transformReference.position);
            }

            yield return t_CycleDelay;
        }

        //Invoking  :   PostProcess
        for (int targetIndex = 0; targetIndex < t_NumberOfTarget; targetIndex++)
        {

            m_ListOfTargetInfos[targetIndex].colorGunTargetAttributeReference.PostProcess();
        }

        StopCoroutine(ControllerForAutoTarget());
    }

    #endregion

    #region Public Callback

    public void PreProcess(bool t_IsUserCanTrigger = false, int t_NumberOfColorGunToBeActivated = 1, float t_TargetingVelocity = 1)
    {

        if (!m_IsAutoTargetControllerRunning)
        {
            //Assigning     :   TargetingVelocity
            m_ListOfTargetInfos = new List<TargetInfo>();

            if (t_TargetingVelocity == 1)
                m_TargetingVelocity = defaultTargetingVelocity;
            else
                m_TargetingVelocity = t_TargetingVelocity;

            //Activating    :   ColorGun
            int t_NumberOfColorGunAttribute = colorGunAttributes.Length;

            m_ListOfActiveColorGunIndex = new List<int>();

            if (t_NumberOfColorGunToBeActivated < t_NumberOfColorGunAttribute)
            {
                //if : We need to randomize

                for (int i = 0; i < t_NumberOfColorGunToBeActivated; i++)
                {

                    bool t_IsFound = false;
                    while (!t_IsFound)
                    {
                        int t_RandomIndex = Random.Range(0, t_NumberOfColorGunAttribute);
                        if (!m_ListOfActiveColorGunIndex.Contains(t_RandomIndex))
                        {
                            Color t_TargetColor = gradientOfTarget.Evaluate(Random.Range(0f, 1f));
                            CreateATarget(i, t_TargetColor);
                            colorGunAttributes[t_RandomIndex].PreProcess(
                                m_ListOfTargetInfos[i].transformReference,
                                t_TargetColor,
                                t_IsUserCanTrigger);
                            m_ListOfActiveColorGunIndex.Add(t_RandomIndex);
                            t_IsFound = true;
                            break;
                        }
                    }
                }
            }
            else
            {

                //Active All Gun
                for (int i = 0; i < t_NumberOfColorGunAttribute; i++)
                {
                    Color t_TargetColor = gradientOfTarget.Evaluate(Random.Range(0f, 1f));
                    CreateATarget(i, t_TargetColor);
                    colorGunAttributes[i].PreProcess(
                        m_ListOfTargetInfos[i].transformReference,
                        t_TargetColor,
                        t_IsUserCanTrigger);
                    m_ListOfActiveColorGunIndex.Add(i);
                }
            }

            if (t_IsUserCanTrigger)
                EnableAimAssist();

            m_IsAutoTargetControllerRunning = true;
            StartCoroutine(ControllerForAutoTarget());
        }
        else
            Debug.LogError("ColorGunController - Already Running : " + gameObject.name);
    }

    public void PostProcess()
    {

        DisableAimAssist();

        int t_NumberOfActiveColorGun = m_ListOfActiveColorGunIndex.Count;
        for (int i = 0; i < t_NumberOfActiveColorGun; i++)
        {
            colorGunAttributes[m_ListOfActiveColorGunIndex[i]].PostProcess();
        }

        m_IsAutoTargetControllerRunning = false;
    }

    public void ChangeMessageInfoOfColorGun(int t_MessageIndex, string t_Message)
    {

        int t_NumberActiveOfColorGunAttribute = m_ListOfActiveColorGunIndex.Count;
        for (int i = 0; i < t_NumberActiveOfColorGunAttribute; i++)
        {

            colorGunAttributes[m_ListOfActiveColorGunIndex[i]].ChangeMessageText(t_MessageIndex, t_Message);
        }
    }

    public List<ColorTurretAttribute> GetListOfActiveColorGunAttribute()
    {

        List<ColorTurretAttribute> t_Result = new List<ColorTurretAttribute>();

        int t_NumberActiveOfColorGunAttribute = m_ListOfActiveColorGunIndex.Count;
        for (int i = 0; i < t_NumberActiveOfColorGunAttribute; i++)
        {

            t_Result.Add(colorGunAttributes[m_ListOfActiveColorGunIndex[i]]);
        }

        return t_Result;
    }

    public List<Transform> GetListOfActiveBullet()
    {

        List<Transform> t_Result = new List<Transform>();

        int t_NumberActiveOfColorGunAttribute = m_ListOfActiveColorGunIndex.Count;
        for (int i = 0; i < t_NumberActiveOfColorGunAttribute; i++)
        {

            List<Transform> t_ListOfActiveBulletFromColorGun = colorGunAttributes[m_ListOfActiveColorGunIndex[i]].GetListOfActiveBullet();
            int t_Counter = t_ListOfActiveBulletFromColorGun.Count;
            for (int j = 0; j < t_Counter; j++)
            {

                t_Result.Add(t_ListOfActiveBulletFromColorGun[j]);
            }
        }

        return t_Result;
    }

    public void EnableAimAssist()
    {

        int t_NumberOfActiveColorGun = m_ListOfTargetInfos.Count;
        for (int i = 0; i < t_NumberOfActiveColorGun; i++)
        {

            m_ListOfTargetInfos[i].colorGunTargetAttributeReference.EnableSpriteRendererOfTarget();
            colorGunAttributes[m_ListOfActiveColorGunIndex[i]].EnableLineRendererForTargetAssist();
        }
    }

    public void DisableAimAssist()
    {

        int t_NumberOfActiveColorGun = m_ListOfTargetInfos.Count;
        for (int i = 0; i < t_NumberOfActiveColorGun; i++)
        {
            m_ListOfTargetInfos[i].colorGunTargetAttributeReference.DisableSpriteRendererOfTarget();
            colorGunAttributes[m_ListOfActiveColorGunIndex[i]].DisableLineRendererForTargetAssist();
        }
    }

    #endregion
}
