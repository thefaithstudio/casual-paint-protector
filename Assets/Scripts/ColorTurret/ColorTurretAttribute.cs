﻿
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Gameplay;

public class ColorTurretAttribute : MonoBehaviour
{

    #region Custom Variables

    private class ColorBulletInfo
    {
        public Transform                    transformReference;
        public ColorBullet                  colorBulletReference;

        public UnityAction<Transform>       OnCollidedWithWorld;
        public Vector3                      targetPosition;

        public ColorBulletInfo(Transform transformReference, Vector3 targetPosition, ColorBullet colorBulletReference) {

            this.transformReference     = transformReference;
            this.targetPosition         = targetPosition;
            this.colorBulletReference   = colorBulletReference;
        }
    }

    private class ColorSplatInfo
    {
        public GameObject   particleObjectReference;
        public float        remainingTimeToPush;

        public ColorSplatInfo(GameObject particleObjectReference, float remainingTimeToPush) {

            this.particleObjectReference    = particleObjectReference;
            this.remainingTimeToPush        = remainingTimeToPush;
        }
    }

    [System.Serializable]
    public class MessageDuringShoot
    {
        public bool     isLoopingMessage;
        [Range(0f,2f)]
        public float    animationSpeed;
        public Color    colorOfTheMessage;
        [Range(0f,1f)]
        public float    timeSlotOnShowingTheMessage;
        public string   message;
    }

    #endregion

    #region Public Variables

    [Header("Reference  :   External")]
    public PreLoadedPrefab  colorSplatParticleContainer;
    public PreLoadedPrefab  colorBulletContainer;
    public UIColorGunInfo   UIColorGunInfoReference;

    [Space(5.0f)]
    [Header("Reference  :   Internal")]
    public Transform        colorGunOperatingPoint;
    public LineRenderer     targetLine;


    [Space(5.0f)]
    [Range(0f,3f)]
    public float        defaultDurationOfColorGunToShoot;
    [Range(1f,25f)]
    public float        defaultForwardVelocityColorBullet = 1;
    public Transform    muzzlePosition;
    public Transform    muzzleHead;

    [Space(5.0f)]
    public MessageDuringShoot[] messageDuringShoot;

    #endregion

    #region Private Variables

    private bool        m_IsColorGunActive;

    private bool        m_IsColorBulletSpawnAllowed;
    private bool        m_IsAimAssistWithLineRendererEnabled;    

    private int         m_NumberOfActiveColorBullet;

    private float       m_DurationToShootColorBullet;
    private float       m_ForwardVelocityColorBullet;
    private float       m_RemainingTimeToShootColorBullet;

    private Color       m_ColorOfTheGun;

    private Transform   m_TargetTransformReference;

    private Quaternion  m_LookRotationForMuzzleHead;
    private Quaternion  m_ModifiedRotationForMuzzleHead;

    private List<ColorBulletInfo> m_ListOfColorBullet;
    private List<ColorSplatInfo> m_ListOfActiveColorSplatInfo;

    #endregion

    #region Configuretion

    private bool IsValidMessageIndex(int t_MessageIndex) {

        if (t_MessageIndex >= 0 && t_MessageIndex < messageDuringShoot.Length)
            return true;

        Debug.LogError("Invalid Message Index : " + t_MessageIndex + ", Must be in the range of (0," + messageDuringShoot.Length + ")");
        return false;
    }

    private void ShowColorSplat(Vector3 t_WorldPosition, Color t_SplatColor)
    {
        GameObject t_NewColorSplat = colorSplatParticleContainer.PullPreloadedPrefab();
        t_NewColorSplat.transform.position = t_WorldPosition;

        ParticleSystem              t_ParticleSystem            = t_NewColorSplat.GetComponent<ParticleSystem>();
        ParticleSystem.MainModule   t_ParticleSystemMainModule  = t_ParticleSystem.main;
        t_ParticleSystemMainModule.startColor    = t_SplatColor;
        t_ParticleSystem.Play();

        m_ListOfActiveColorSplatInfo.Add(new ColorSplatInfo(
                t_NewColorSplat,
                1f
            ));
        
    }

    private IEnumerator ControllerForColorBulletMovement() {

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        m_NumberOfActiveColorBullet = 0;
        m_ListOfColorBullet         = new List<ColorBulletInfo>();

        int         t_PlayerIDForAttacker              = GameplayController.Instance.GetPlayerIDForAttacker();
        float       t_CoinEarnMultiplier    = PlayerStateController.Instance.GetCurrentStateOfCoinEarn();
        float       t_AbsoluteVelocity;
        float       t_AngulerVelocity = 0.1f;

        Vector3     t_ModifiedPositon;
        Quaternion  t_LookRotation;
        Quaternion  t_ModifiedRotation;

        while (m_IsColorGunActive) {

            t_AbsoluteVelocity = m_ForwardVelocityColorBullet * Time.deltaTime;

            for (int bulletIndex = 0; bulletIndex < m_NumberOfActiveColorBullet; bulletIndex++) {

                if (Vector3.Distance(m_ListOfColorBullet[bulletIndex].transformReference.position, m_ListOfColorBullet[bulletIndex].targetPosition) <= 0.1f)
                {
                    ShowColorSplat(
                        m_ListOfColorBullet[bulletIndex].transformReference.position,
                        m_ColorOfTheGun);

                    if (t_PlayerIDForAttacker == 0)
                    {
                        //It is player
                        GameplayController.Instance.EarnCoinToPlayer(false);
                    }

                    UIStateController.Instance.UpdatePlayerRankList(t_PlayerIDForAttacker, Mathf.CeilToInt(15 * t_CoinEarnMultiplier));
                    DecyleUsingSpriteMaskController.Instance.CreateSplatterWtthGradient(
                            m_ListOfColorBullet[bulletIndex].transformReference.position,
                            m_ListOfColorBullet[bulletIndex].colorBulletReference.GetColorBulletGradient()
                        );

                    colorBulletContainer.PushPreloadedPrefab(m_ListOfColorBullet[bulletIndex].transformReference.gameObject);
                    m_ListOfColorBullet[bulletIndex].colorBulletReference.PostProess();
                    m_ListOfColorBullet.RemoveAt(bulletIndex);
                    m_ListOfColorBullet.TrimExcess();

                    m_NumberOfActiveColorBullet--;
                }
                else {

                    t_ModifiedPositon = Vector3.MoveTowards(
                            m_ListOfColorBullet[bulletIndex].transformReference.position,
                            m_ListOfColorBullet[bulletIndex].targetPosition,
                            t_AbsoluteVelocity
                        );

                    m_ListOfColorBullet[bulletIndex].transformReference.position = t_ModifiedPositon;

                    t_LookRotation = Quaternion.LookRotation(m_ListOfColorBullet[bulletIndex].targetPosition - m_ListOfColorBullet[bulletIndex].transformReference.position);
                    t_ModifiedRotation = Quaternion.Slerp(
                            m_ListOfColorBullet[bulletIndex].transformReference.rotation,
                            t_LookRotation,
                            t_AngulerVelocity
                        );
                    m_ListOfColorBullet[bulletIndex].transformReference.rotation = t_ModifiedRotation;
                }
            }

            yield return t_CycleDelay;
        }

        //Post Processing
        m_NumberOfActiveColorBullet = m_ListOfColorBullet.Count;
        for (int bulletIndex = 0; bulletIndex < m_NumberOfActiveColorBullet; bulletIndex++) {

            ShowColorSplat(
                        m_ListOfColorBullet[bulletIndex].transformReference.position,
                        m_ColorOfTheGun);

            colorBulletContainer.PushPreloadedPrefab(m_ListOfColorBullet[bulletIndex].transformReference.gameObject);
            m_ListOfColorBullet[bulletIndex].colorBulletReference.PostProess();
            m_ListOfColorBullet.RemoveAt(bulletIndex);
            m_ListOfColorBullet.TrimExcess();

            m_NumberOfActiveColorBullet--;
        }

        StopCoroutine(ControllerForColorBulletMovement());
    }

    private IEnumerator ControllerForColorBulletSpawn()
    {

        //Shoot
        GameObject t_NewColorBullet = colorBulletContainer.PullPreloadedPrefab();

        Transform t_NewColorBulletTransformReference = t_NewColorBullet.transform;
        t_NewColorBulletTransformReference.rotation = muzzlePosition.rotation;

        m_ListOfColorBullet.Add(new ColorBulletInfo(
                t_NewColorBulletTransformReference,
                m_TargetTransformReference.position,
                t_NewColorBullet.GetComponent<ColorBullet>()
            ));
        m_ListOfColorBullet[m_NumberOfActiveColorBullet].colorBulletReference.PreProcess(
            m_ColorOfTheGun,
            muzzlePosition.position,
            OnColorBulletCollidedWithWorld
        );
        m_NumberOfActiveColorBullet++;

        //Delay
        float t_CycleLength = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        int     t_NumberOfMessageDuringShoot    = messageDuringShoot.Length;
        bool[]  t_AnimationCheckTrigger         = new bool[t_NumberOfMessageDuringShoot];
        float   t_TimeProgression;

        while (m_RemainingTimeToShootColorBullet > 0) {

            m_RemainingTimeToShootColorBullet -= t_CycleLength;

            t_TimeProgression = 1f - (m_RemainingTimeToShootColorBullet / m_DurationToShootColorBullet);
            //Debug.Log("TimeProgression : " + t_TimeProgression);
            for (int i = 0; i < t_NumberOfMessageDuringShoot; i++) {

                if (!t_AnimationCheckTrigger[i] && (t_TimeProgression >= messageDuringShoot[i].timeSlotOnShowingTheMessage)) {

                    t_AnimationCheckTrigger[i] = true;
                    UIColorGunInfoReference.ShowMessage(
                            messageDuringShoot[i].message,
                            messageDuringShoot[i].animationSpeed,
                            messageDuringShoot[i].colorOfTheMessage,
                            messageDuringShoot[i].isLoopingMessage
                        );
                    //Show Animation
                }
            }

            yield return t_CycleDelay;

        }

        StopCoroutine(ControllerForColorBulletSpawn());
        m_IsColorBulletSpawnAllowed = true;
    }

    private IEnumerator ControllerForColorSplatLifeCycle() {

        float                   t_CycleLength   = 0.1f;
        WaitForSeconds          t_CycleDelay    = new WaitForSeconds(t_CycleLength);

        int                     t_NumberOfActiveColorSplat = 0;

        m_ListOfActiveColorSplatInfo = new List<ColorSplatInfo>();

        while (m_IsColorGunActive) {

            t_NumberOfActiveColorSplat = m_ListOfActiveColorSplatInfo.Count;
            for (int i = 0; i < t_NumberOfActiveColorSplat; i++) {

                m_ListOfActiveColorSplatInfo[i].remainingTimeToPush -= t_CycleLength;

                if (m_ListOfActiveColorSplatInfo[i].remainingTimeToPush <= 0) {

                    colorSplatParticleContainer.PushPreloadedPrefab(m_ListOfActiveColorSplatInfo[i].particleObjectReference);

                    m_ListOfActiveColorSplatInfo.RemoveAt(i);
                    m_ListOfActiveColorSplatInfo.TrimExcess();
                    t_NumberOfActiveColorSplat--;
                }
            }

            yield return t_CycleDelay;
        }

        t_NumberOfActiveColorSplat = m_ListOfActiveColorSplatInfo.Count;
        for (int i = 0; i < t_NumberOfActiveColorSplat; i++) {

            colorSplatParticleContainer.PushPreloadedPrefab(m_ListOfActiveColorSplatInfo[i].particleObjectReference);
        }

        StopCoroutine(ControllerForColorSplatLifeCycle());
    }

    #endregion

    #region Public Callback

    public bool IsShootingColorBulletAllowed()
    {
        if (m_IsColorGunActive && m_IsColorBulletSpawnAllowed)
            return true;

        return false;
    }

    public int GetNumberOfActiveColorBullet()
    {
        return m_NumberOfActiveColorBullet;
    }       

    public void PreProcess(Transform t_TargetTransformReference, Color t_ColorOfTheGun, bool t_IsUserCanTrigger = false, float t_DurationToShootColorBullet = -1, float t_ForwardVelocityColorBullet = 1)
    {
        m_TargetTransformReference = t_TargetTransformReference;
        m_ColorOfTheGun = t_ColorOfTheGun;
        t_ColorOfTheGun.a = 0.4f;
        targetLine.material.SetColor("_BaseColor", t_ColorOfTheGun);

        targetLine.SetPosition(0, muzzlePosition.position);
        targetLine.SetPosition(0, muzzlePosition.position);

        if(m_IsAimAssistWithLineRendererEnabled)
            targetLine.enabled = true;

        if (t_DurationToShootColorBullet == -1)
            m_DurationToShootColorBullet = defaultDurationOfColorGunToShoot;
        else
            m_DurationToShootColorBullet = t_DurationToShootColorBullet;

        if (t_ForwardVelocityColorBullet == 1)
            m_ForwardVelocityColorBullet = defaultForwardVelocityColorBullet;
        else
            m_ForwardVelocityColorBullet = t_ForwardVelocityColorBullet;
        
        UIColorGunInfoReference.PreProcess(
            transform.position,
            t_IsUserCanTrigger ? new UnityAction(delegate 
                { CharacterControllerForAttacker.Instance.SetTargetForCharacter(this);
                }) 
                : new UnityAction(delegate {  }));

        if (!m_IsColorGunActive) {

            m_IsColorBulletSpawnAllowed = true;

            m_IsColorGunActive = true;
            StartCoroutine(ControllerForColorBulletMovement());
            StartCoroutine(ControllerForColorSplatLifeCycle());
        }
    }

    public void PostProcess()
    {
        UIColorGunInfoReference.PostProcess();
        m_IsColorGunActive = false;
        targetLine.enabled = false;
    }

    public void RotateMuzzleHeadTowardsTarget(Vector3 t_Target, float t_AngulerVelocity = 0.5f)
    {
        m_LookRotationForMuzzleHead = Quaternion.LookRotation(t_Target - muzzleHead.position);
        m_ModifiedRotationForMuzzleHead = Quaternion.Slerp(
                muzzleHead.rotation,
                m_LookRotationForMuzzleHead,
                t_AngulerVelocity
            );
        muzzleHead.rotation = m_ModifiedRotationForMuzzleHead;

        if (m_IsAimAssistWithLineRendererEnabled) {

            targetLine.SetPosition(0, muzzlePosition.position);
            targetLine.SetPosition(1, t_Target);
        }
        
    }

    public void ShootColor() {
        
        if (IsShootingColorBulletAllowed()) {

            m_IsColorBulletSpawnAllowed         = false;
            m_RemainingTimeToShootColorBullet   = m_DurationToShootColorBullet;

            StartCoroutine(ControllerForColorBulletSpawn());
        }
    }

    public List<Transform> GetListOfActiveBullet() {

        List<Transform> t_Result = new List<Transform>();

        int t_NumberOfActiveBullet = m_ListOfColorBullet.Count;
        for (int bulletIndex = 0; bulletIndex < t_NumberOfActiveBullet; bulletIndex++) {

            t_Result.Add(m_ListOfColorBullet[bulletIndex].transformReference);
        }

        return t_Result;
    }

    public Transform GetTransformOfOperationPoint() {

        return colorGunOperatingPoint;
    }

    public Vector3 GetPositionOfOperatingPoint() {

        return colorGunOperatingPoint.position;
    }

    public bool IsYourPositionAtOperatingPoint(Vector3 t_YourPosition) {

        if (Vector3.Distance(t_YourPosition, colorGunOperatingPoint.position) <= 0.1f) {

            return true;
        }

        return false;
    }

    public void OnColorBulletCollidedWithWorld(Transform t_ColorBulletTransformReference)
    {
        for (int bulletIndex = 0; bulletIndex < m_NumberOfActiveColorBullet; bulletIndex++) {

            if (t_ColorBulletTransformReference == m_ListOfColorBullet[bulletIndex].transformReference) {

                ShowColorSplat(
                        m_ListOfColorBullet[bulletIndex].transformReference.position,
                        m_ColorOfTheGun);

                int t_PlayerIDAsDefender = GameplayController.Instance.GetPlayerIDForDefender();
                if (t_PlayerIDAsDefender == 0) {
                    //It is player
                    HapticFeedbackController.Instance.TapPopVibrate();
                    GameplayController.Instance.EarnCoinToPlayer(true);
                }
                UIStateController.Instance.UpdatePlayerRankList(t_PlayerIDAsDefender, Mathf.CeilToInt(10 * PlayerStateController.Instance.GetCurrentStateOfCoinEarn()));

                colorBulletContainer.PushPreloadedPrefab(m_ListOfColorBullet[bulletIndex].transformReference.gameObject);
                m_ListOfColorBullet[bulletIndex].colorBulletReference.PostProess();
                m_ListOfColorBullet.RemoveAt(bulletIndex);
                m_ListOfColorBullet.TrimExcess();

                m_NumberOfActiveColorBullet--;
            }
        }

    }

    public void EnableLineRendererForTargetAssist() {

        m_IsAimAssistWithLineRendererEnabled = true;
        targetLine.enabled = true;
    }

    public void DisableLineRendererForTargetAssist()
    {
        m_IsAimAssistWithLineRendererEnabled = false;
        targetLine.enabled = false;
    }

    #endregion

    #region Public Callback :   ChangeMessage

    public void ChangeMessageText(int t_MessageIndex, string t_Message) {

        if (IsValidMessageIndex(t_MessageIndex)) {

            messageDuringShoot[t_MessageIndex].message = t_Message;
        }
    }

    #endregion
}
