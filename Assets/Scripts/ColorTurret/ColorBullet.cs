﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class ColorBullet : MonoBehaviour
{
    #region Public Variables

    public MeshRenderer     meshRendererReference;

    [Space(5.0f)]
    [Range(0f,0.1f)]
    public float colorDistortion = 0f;
    public ParticleSystem   colorTrailParticle;

    #endregion

    #region Private Variables

    private     Color                   m_ColorBullet;
    private     Gradient                m_ColorGradientOfBullet;
    private     UnityAction<Transform>  OnColorBulletCollidedWithWorld;

    #endregion

    #region Mono Behaviour

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Tag_Shield") {

            if (OnColorBulletCollidedWithWorld != null)
                OnColorBulletCollidedWithWorld.Invoke(transform);
        }
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForPostProcessing() {

        meshRendererReference.enabled           = false;
        colorTrailParticle.Stop();

        yield return null;

        StopCoroutine(ControllerForPostProcessing());
    }

    #endregion

    #region Public Callback

    public void PreProcess(Color t_ColorOfBullet, Vector3 t_InitialPosition, UnityAction<Transform> OnColorBulletCollidedWithWorld = null) {

        m_ColorBullet = t_ColorOfBullet;
        transform.position = t_InitialPosition;
        this.OnColorBulletCollidedWithWorld = OnColorBulletCollidedWithWorld;
        

        m_ColorGradientOfBullet     = new Gradient();
        GradientColorKey[] colorKey = new GradientColorKey[2];
        GradientAlphaKey[] alphaKey = new GradientAlphaKey[2];

        colorKey[0].color = new Color(
                m_ColorBullet.r - (m_ColorBullet.r * colorDistortion) < 0 ? 0f : m_ColorBullet.r - (m_ColorBullet.r * colorDistortion),
                m_ColorBullet.g - (m_ColorBullet.g * colorDistortion) < 0 ? 0f : m_ColorBullet.g - (m_ColorBullet.g * colorDistortion),
                m_ColorBullet.b - (m_ColorBullet.b * colorDistortion) < 0 ? 0f : m_ColorBullet.b - (m_ColorBullet.b * colorDistortion),
                1f
            );
        colorKey[0].time = 0f;

        colorKey[1].color = new Color(
                m_ColorBullet.r + (m_ColorBullet.r * colorDistortion) > 1 ? 1f : m_ColorBullet.r + (m_ColorBullet.r * colorDistortion),
                m_ColorBullet.g + (m_ColorBullet.g * colorDistortion) > 1 ? 1f : m_ColorBullet.g + (m_ColorBullet.g * colorDistortion),
                m_ColorBullet.b + (m_ColorBullet.b * colorDistortion) > 1 ? 1f : m_ColorBullet.b + (m_ColorBullet.b * colorDistortion),
                1f
            );
        colorKey[1].time    = 1f;

        alphaKey[0].alpha   = 1f;
        alphaKey[0].time    = 0f;

        alphaKey[1].alpha   = 1f;
        alphaKey[1].time    = 1f;

        m_ColorGradientOfBullet.SetKeys(colorKey, alphaKey);
        
        meshRendererReference.material.SetColor("_BaseColor", m_ColorGradientOfBullet.Evaluate(Random.Range(0f,1f)));

        ParticleSystem.MainModule   t_MainModuleReference   = colorTrailParticle.main;
        t_MainModuleReference.startColor                    = m_ColorGradientOfBullet;

        meshRendererReference.enabled = true;
        colorTrailParticle.Play();
    }

    public void PostProess() {

        StartCoroutine(ControllerForPostProcessing());
    }

    public Gradient GetColorBulletGradient() {

        return m_ColorGradientOfBullet;
    }

    #endregion
}
