﻿namespace com.faithstudio.Camera
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class CameraFocus : MonoBehaviour
    {
        #region Public Variables

        public Camera cameraReference;
        #endregion

        #region Private Variables

        private List<Transform> m_ListOfFocusingObjects;
        private Transform m_CameraTransformReference;
        private Quaternion m_InitialRotationOfCamera;
        private Quaternion m_ModifiedLookAtDirection;

        private Vector3 m_InitialPositionOfCamera;
        private Vector3 m_CameraFocusingOffSet;

        private bool m_IsCameraFocusingControllerRunning;
        private bool m_IsCameraFocusing;
        private bool m_IsOrthographicCamera;

        private float m_InitialFieldOfView;
        private float m_InitialOrthographicSize;
        [SerializeField]
        private float m_RestoringFocusingValue;
        private float m_FocusingValueWhenUnfocusStart;
        private float m_FocusSpeed;
        private float m_UnFocusSpeed;

        //------------------
        private bool m_IsCameraLookAtRotationAllowed;
        private bool m_IsCameraFreeDisplacementAllowed;

        private Bounds m_Bounds;

        private float m_MinZoom;
        private float m_MaxZoom;
        private float m_ModifiedZoom;

        private Vector3 m_CameraPositionOffSet;
        private Vector3 m_CameraFollowingSpeed;
        #endregion

        #region Mono Behaviour

        private void Awake()
        {
            m_CameraTransformReference = cameraReference.GetComponent<Transform>();
            m_InitialPositionOfCamera = m_CameraTransformReference.position;
            m_InitialRotationOfCamera = m_CameraTransformReference.rotation;
            m_InitialFieldOfView = cameraReference.fieldOfView;
            m_InitialOrthographicSize = cameraReference.orthographicSize;
        }

        #endregion

        #region Configuretion


        private IEnumerator ControllerForCameraFocusing()
        {

            float t_CycleLength = 0.0167f;
            WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

            Quaternion t_LookAtDirection;

            int t_NumberOfTarget = m_ListOfFocusingObjects.Count;
            float t_DeltaTime;
            float t_AbsoluteFocusSpeed;
            float t_AbsoluteUnFocusSpeed;
            float t_UnfocusBoundary = m_UnFocusSpeed * 1f;

            //-----------
            Vector3 t_CenterPointOfFocus;
            Vector3 t_ModifiedCenterPointOfFocus;
            float t_SmoothTime = 0.5f;
            float t_AvarageOfBoundSize;

            while (m_IsCameraFocusingControllerRunning)
            {


                t_DeltaTime = Time.deltaTime;
                t_AbsoluteFocusSpeed = m_FocusSpeed * t_DeltaTime;
                t_AbsoluteUnFocusSpeed = m_UnFocusSpeed * t_DeltaTime;

                if (m_IsCameraFocusing)
                {
                    //Calculate : Camera  Boundary
                    m_Bounds = new Bounds();
                    for (int counter = 0; counter < t_NumberOfTarget; counter++)
                    {
                        m_Bounds.Encapsulate(m_ListOfFocusingObjects[counter].position);
                    }

                    //Calculate : Camera Zooming
                    t_AvarageOfBoundSize = ((m_Bounds.size.x + m_Bounds.size.y + m_Bounds.size.z) / 3.0f);
                    m_ModifiedZoom = Mathf.Lerp(m_MaxZoom, m_MinZoom, t_AvarageOfBoundSize / m_MinZoom);
                    if (m_IsOrthographicCamera)
                    {
                        cameraReference.orthographicSize = Mathf.Lerp(cameraReference.orthographicSize, m_ModifiedZoom, t_DeltaTime);
                    }
                    else
                    {
                        cameraReference.fieldOfView = Mathf.Lerp(cameraReference.fieldOfView, m_ModifiedZoom, t_DeltaTime);
                    }

                    //Calculate : Camera Rotation While Zooming
                    if (m_IsCameraLookAtRotationAllowed) {

                        t_LookAtDirection = Quaternion.LookRotation((m_Bounds.center + m_CameraFocusingOffSet) - m_CameraTransformReference.position);
                        m_ModifiedLookAtDirection = Quaternion.Slerp(m_ModifiedLookAtDirection, t_LookAtDirection, 15f * t_AbsoluteFocusSpeed);
                        m_CameraTransformReference.rotation = m_ModifiedLookAtDirection;
                    }
                    
                    //Calculate : Camera Displacement
                    if (m_IsCameraFreeDisplacementAllowed)
                    {

                        t_CenterPointOfFocus = m_Bounds.center;
                        t_ModifiedCenterPointOfFocus = t_CenterPointOfFocus + m_CameraPositionOffSet;
                        m_CameraTransformReference.position = Vector3.SmoothDamp(
                            m_CameraTransformReference.position,
                            t_ModifiedCenterPointOfFocus,
                            ref m_CameraFollowingSpeed,
                            t_SmoothTime);
                    }
                }
                else
                {

                    if (m_IsOrthographicCamera)
                    {
                        m_ModifiedZoom = cameraReference.orthographicSize;
                    }
                    else
                    {

                        m_ModifiedZoom = cameraReference.fieldOfView;
                    }

                    m_ModifiedZoom = Mathf.Lerp(
                                m_ModifiedZoom,
                                m_RestoringFocusingValue,
                                t_AbsoluteUnFocusSpeed
                            );

                    if (m_IsCameraFreeDisplacementAllowed)
                    {

                        m_CameraTransformReference.position = Vector3.SmoothDamp(
                            m_CameraTransformReference.position,
                            m_InitialPositionOfCamera,
                            ref m_CameraFollowingSpeed,
                            t_SmoothTime);
                    }

                    if ((m_IsCameraFreeDisplacementAllowed ? (Vector3.Distance(m_CameraTransformReference.position, m_InitialPositionOfCamera) <= t_UnfocusBoundary) : true) && (Mathf.Abs(m_RestoringFocusingValue - m_ModifiedZoom) <= t_UnfocusBoundary))
                    {
                        if (m_IsOrthographicCamera)
                            cameraReference.orthographicSize = m_RestoringFocusingValue;
                        else
                            cameraReference.fieldOfView = m_RestoringFocusingValue;

                        m_CameraTransformReference.rotation = m_InitialRotationOfCamera;

                        m_IsCameraFocusingControllerRunning = false;
                        break;
                    }
                    else
                    {
                        if (m_IsOrthographicCamera)
                            cameraReference.orthographicSize = m_ModifiedZoom;
                        else
                            cameraReference.fieldOfView = m_ModifiedZoom;

                        m_ModifiedLookAtDirection = m_CameraTransformReference.rotation;
                        m_ModifiedLookAtDirection = Quaternion.Slerp(

                                    m_ModifiedLookAtDirection,
                                    m_InitialRotationOfCamera,
                                    1f - ((m_RestoringFocusingValue - m_ModifiedZoom) / (m_RestoringFocusingValue - m_FocusingValueWhenUnfocusStart))
                                );
                        m_CameraTransformReference.rotation = m_ModifiedLookAtDirection;

                    }
                }


                yield return t_CycleDelay;
            }

            StopCoroutine(ControllerForCameraFocusing());
        }

        #endregion

        #region Public Callback

        public void FocusCamera(List<Transform> t_ListOfFocusingObjects, Vector3 t_CameraFocusingOffset, float t_FocusingValue = 0.5f, float t_FocusSpeed = 0.1f, float t_UnFocusSpeed = 0.01f)
        {

            FocusCamera(t_ListOfFocusingObjects, t_CameraFocusingOffset, Vector3.zero, t_FocusingValue, t_FocusSpeed, t_UnFocusSpeed);
        }

        public void FocusCamera(List<Transform> t_ListOfFocusingObjects, Vector3 t_CameraFocusingOffset, Vector3 t_CameraPositionOffset, float t_FocusingValue = 0.5f, float t_FocusSpeed = 0.1f, float t_UnFocusSpeed = 0.01f)
        {

            m_ListOfFocusingObjects = new List<Transform>(t_ListOfFocusingObjects);
            m_CameraFocusingOffSet = t_CameraFocusingOffset;
            m_CameraPositionOffSet = t_CameraPositionOffset;
            m_FocusSpeed = t_FocusSpeed;
            m_UnFocusSpeed = t_UnFocusSpeed;

            m_IsCameraFocusing = true;
            m_IsCameraLookAtRotationAllowed     = m_CameraFocusingOffSet == Vector3.zero ? false : true;
            m_IsCameraFreeDisplacementAllowed   = m_CameraPositionOffSet == Vector3.zero ? false : true;
            m_ModifiedLookAtDirection = m_CameraTransformReference.rotation;

            if (cameraReference.orthographic)
            {
                m_IsOrthographicCamera = true;
                m_MinZoom = m_InitialOrthographicSize;
                m_MaxZoom = m_InitialOrthographicSize * (1f - t_FocusingValue);

                m_RestoringFocusingValue = m_InitialOrthographicSize;
            }
            else
            {

                m_IsOrthographicCamera = false;
                m_MinZoom = m_InitialFieldOfView;
                m_MaxZoom = m_InitialFieldOfView * (1f - t_FocusingValue);

                m_RestoringFocusingValue = m_InitialFieldOfView;
            }

            if (!m_IsCameraFocusingControllerRunning)
            {

                m_IsCameraFocusingControllerRunning = true;
                StartCoroutine(ControllerForCameraFocusing());
            }
        }

        public void UnfocusCamera()
        {

            if (m_IsOrthographicCamera)
            {

                m_FocusingValueWhenUnfocusStart = cameraReference.orthographicSize;
            }
            else
            {

                m_FocusingValueWhenUnfocusStart = cameraReference.fieldOfView;
            }

            m_IsCameraFocusing = false;
        }

        #endregion
    }
}
