﻿namespace com.faithstudio.Camera {

    using System.Collections;
    using UnityEngine;

    public class CameraShake : MonoBehaviour
    {
        #region Public Variables

        public Transform cameraContainerTransformReference;

        [Space(5.0f)]
        [Range(0f, 10f)]
        public float cameraShakeSpeed = 1f;
        [Range(0f, 10f)]
        public float defaultDurationOfCameraShake = 2.5f;

        [Space(5.0f)]
        [Header("Shaking Points")]
        [Range(0f, 5f)]
        public float cameraShakeOnXAxis = 1f;
        [Range(0f, 5f)]
        public float cameraShakeOnYAxis = 1f;
        [Range(0f, 5f)]
        public float cameraShakeOnZAxis = 1f;

        #endregion

        #region Private Variables


        private Vector3 m_VarientInitialPositionOfCameraContainer;
        private Vector3 m_InitialPositionOfCameraContainer;

        private bool m_IsCameraShakeControllerRunning;
        private bool m_IsRepetativeShakeEnabled;

        private float m_AbsoluteCameraShakeDuration;
        private float m_RemainingTimeForCameraShake;
        private float m_AbsluteCameraShakingSpeed;

        #endregion

        #region MonoBehaviour

        private void Awake()
        {
            Initialization();
        }

        #endregion

        #region Configuretion

        private void Initialization()
        {

            if (cameraContainerTransformReference == null)
                cameraContainerTransformReference = transform;

            m_InitialPositionOfCameraContainer = cameraContainerTransformReference.localPosition;
            m_VarientInitialPositionOfCameraContainer = cameraContainerTransformReference.localPosition;

        }

        private IEnumerator ControllerForCameraShaking()
        {

            Vector3 t_TargetedPosition = Vector3.zero;
            Vector3 t_ModifiedPosition;

            float t_CurrentDistance = 0f;

            float t_CycleLength = 0.0167f;
            WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);
            WaitForEndOfFrame t_WaitForEndOfFrame = new WaitForEndOfFrame();

            while (m_RemainingTimeForCameraShake > 0)
            {

                if (t_CurrentDistance <= 0.1f)
                {

                    t_TargetedPosition = new Vector3(
                            m_VarientInitialPositionOfCameraContainer.x + Random.Range(-cameraShakeOnXAxis, cameraShakeOnXAxis),
                            m_VarientInitialPositionOfCameraContainer.y + Random.Range(-cameraShakeOnYAxis, cameraShakeOnYAxis),
                            m_VarientInitialPositionOfCameraContainer.z + Random.Range(-cameraShakeOnZAxis, cameraShakeOnZAxis)
                        );
                }

                t_ModifiedPosition = Vector3.Lerp(
                        m_VarientInitialPositionOfCameraContainer,
                        t_TargetedPosition,
                        m_AbsluteCameraShakingSpeed * Time.deltaTime
                    );
                cameraContainerTransformReference.localPosition = t_ModifiedPosition;

                t_CurrentDistance = Vector3.Distance(t_ModifiedPosition, t_TargetedPosition);


                if (!m_IsRepetativeShakeEnabled)
                {
                    yield return t_CycleDelay;
                    m_RemainingTimeForCameraShake -= t_CycleLength;
                }
                else
                {
                    yield return t_WaitForEndOfFrame;
                }
            }

            //RestoringPoint
            while (Vector3.Distance(cameraContainerTransformReference.localPosition, m_InitialPositionOfCameraContainer) >= 0.1f)
            {

                t_ModifiedPosition = Vector3.Lerp(
                        cameraContainerTransformReference.localPosition,
                        m_InitialPositionOfCameraContainer,
                        m_AbsluteCameraShakingSpeed * Time.deltaTime
                    );
                cameraContainerTransformReference.localPosition = t_ModifiedPosition;

                yield return t_CycleDelay;
            }

            StopCoroutine(ControllerForCameraShaking());
            m_IsCameraShakeControllerRunning = false;
        }

        #endregion

        #region Public Callback

        public void ShowCameraShake()
        {

            ShowCameraShake(defaultDurationOfCameraShake, cameraShakeSpeed, false);
        }

        public void ShowCameraShake(float t_Duration, bool t_RepetativeShake)
        {

            ShowCameraShake(defaultDurationOfCameraShake, cameraShakeSpeed, t_RepetativeShake);
        }

        public void ShowCameraShake(float t_Duration, float t_CameraShakeSpeed, bool t_RepetativeShake)
        {

            if (!m_IsCameraShakeControllerRunning)
            {
                m_AbsluteCameraShakingSpeed = t_CameraShakeSpeed;
                m_AbsoluteCameraShakeDuration = t_Duration;
                m_RemainingTimeForCameraShake = t_Duration;
                m_IsRepetativeShakeEnabled = t_RepetativeShake;

                m_IsCameraShakeControllerRunning = true;
                StartCoroutine(ControllerForCameraShaking());
            }
            else
            {

                Debug.LogWarning("CameraShake Controller already running");
            }
        }

        public void StopCameraShake()
        {

            m_RemainingTimeForCameraShake = 0f;
        }

        public void ChangeShakingValue(float t_ShakeSpeed)
        {
            m_AbsluteCameraShakingSpeed = t_ShakeSpeed;
        }

        #endregion
    }
}
