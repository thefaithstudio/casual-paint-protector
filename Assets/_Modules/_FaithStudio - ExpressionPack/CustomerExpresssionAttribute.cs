﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CustomerExpresssionAttribute : MonoBehaviour
{
    #region Public Variables

    public Image    expressionImageReference;
    public Animator animatorReference;
    public ParticleSystem expressionparticle;

    #endregion

    #region Private Variables
    

    #endregion

    #region Configuretion

    private IEnumerator ControllerForDisposing(Transform t_FollowedCustomer, Vector3 t_Displacemenet) {

        float t_CycleLength = 0.0167f;
        float t_RemainingTimeLeftForAnimationToEnd = 1f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        Transform t_TransformReference = transform;
        Vector3 t_ModifiedPosition;

        expressionparticle.Stop();
        animatorReference.SetTrigger("DISAPPEAR");

        while (t_RemainingTimeLeftForAnimationToEnd > 0f) {


            if (t_FollowedCustomer != null) {
                t_ModifiedPosition = t_FollowedCustomer.position + t_Displacemenet;
                t_TransformReference.position = t_ModifiedPosition;
            }
            
            yield return t_CycleDelay;
            t_RemainingTimeLeftForAnimationToEnd -= t_CycleLength;
        }
        
        StopCoroutine(ControllerForDisposing(null,Vector3.zero));

        Destroy(gameObject);
    }

    #endregion

    #region Public Callback

    public void ChangeExpressionSprite(Sprite t_ExpressionSprite, bool t_ShowChangeAnimation) {

        expressionImageReference.sprite = t_ExpressionSprite;

        if (t_ShowChangeAnimation) {
            expressionparticle.Clear();
            expressionparticle.Play();
            animatorReference.SetTrigger("SWITCH");
        }
            
    }

    public void PreProcess(Sprite t_ExpressionSprite) {

        expressionparticle.Play();
        animatorReference.SetTrigger("APPEAR");
        ChangeExpressionSprite(t_ExpressionSprite,false);
    }

    public void PostProcess(Transform t_FollowedCustomer, Vector3 t_Displacemenet) {

        StartCoroutine(ControllerForDisposing(t_FollowedCustomer, t_Displacemenet));
    }
    
    #endregion

}
