﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

public class CustomerExpressionController : MonoBehaviour
{

    public static CustomerExpressionController Instance;

    #region Custom Variables

    [System.Serializable]
    public struct Expression
    {
        public Sprite expressionSprite;
    }

    [System.Serializable]
    public struct ActiveExpression
    {
        public bool flagedForDispose;

        public float        duration;
        public Vector3      displacement;
        public Transform    customerTransformReference;

        public Sprite expressionSprite;
        public Transform  expressionUI;
        public CustomerExpresssionAttribute expressionAttribute;
    }

    #endregion

    #region Public Variables

    public Transform    customerExpressionParent;
    public GameObject   customerExpressionPrefab;

    [Space(5.0f)]
    [Header("Reference : Game Specefic Expression")]
    public Sprite[] expressionsOfHappy;
    public Sprite[] expressionOfAngry;

    [Space(5.0f)]
    [Header("Reference : Expression")]
    public Expression[] customerExpression;

    #endregion

    #region Private Variables

    private Transform m_TransformReference;

    public List<ActiveExpression> m_ListOfActiveExpression;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {

        Instance = this;

        m_TransformReference = transform;
        m_ListOfActiveExpression = new List<ActiveExpression>();
    }

    #endregion

    #region Configuretion

    private bool m_IsCustomerExpressionControllerRunning = false;

    private Transform   m_TargetedPosition;
    private Vector3     m_Displacement;
    private float       m_Duration;

    private int IsExpressionAlreadyCreatedForCustomer(Transform t_CustomerTransformReference) {
        
        int t_NumberOfActiveExpression = m_ListOfActiveExpression.Count;
        for (int expressionIndex = 0; expressionIndex < t_NumberOfActiveExpression; expressionIndex++) {

            if (m_ListOfActiveExpression[expressionIndex].customerTransformReference == t_CustomerTransformReference) {
                return expressionIndex;
            }
        }

        return -1;
    }

    private IEnumerator ControllerForLifeCycleOfCustomerExpression() {

        float t_CycleLength = 0.005f; 
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        //----------------------------
        int t_NumberOfActiveExpression;
        ActiveExpression t_ModifiedActiveExpression;
        Vector3 t_ModifiedPositionForExpressionUI;
        //----------------------------
        
        List<int> t_IndexOfActiveExpressionToBeRecycled;

        while (m_IsCustomerExpressionControllerRunning) {

            t_NumberOfActiveExpression              = m_ListOfActiveExpression.Count;
            t_IndexOfActiveExpressionToBeRecycled   = new List<int>();

            // Decrement & Dispose Call
            for (int expressionIndex = 0; expressionIndex < t_NumberOfActiveExpression; expressionIndex++) {

                t_ModifiedActiveExpression = m_ListOfActiveExpression[expressionIndex];

                if (m_ListOfActiveExpression[expressionIndex].duration <= 0)
                {
                    t_IndexOfActiveExpressionToBeRecycled.Add(expressionIndex);
                }
                else {

                    //Decrementing Time
                    t_ModifiedActiveExpression.duration -= t_CycleLength;

                    //Changing Position of UI
                    if (m_ListOfActiveExpression[expressionIndex].flagedForDispose)
                    {
                        // Customer Instance already destroyed but if the expression still alive
                        t_IndexOfActiveExpressionToBeRecycled.Add(expressionIndex);
                    }
                    else {
                        t_ModifiedPositionForExpressionUI = t_ModifiedActiveExpression.customerTransformReference.position + t_ModifiedActiveExpression.displacement;
                        t_ModifiedActiveExpression.expressionUI.position = t_ModifiedPositionForExpressionUI;
                    }
                }

                m_ListOfActiveExpression[expressionIndex] = t_ModifiedActiveExpression;
            }

            //Dispose
            if (t_IndexOfActiveExpressionToBeRecycled.Count > 0) {

                int t_SelectedIndex;
                int t_NumberOfExpressionToBeDisposed = t_IndexOfActiveExpressionToBeRecycled.Count;

                //Called : PostProcess
                for (int loopCounter = 0; loopCounter < t_NumberOfExpressionToBeDisposed; loopCounter++) {

                    t_SelectedIndex = t_IndexOfActiveExpressionToBeRecycled[loopCounter] - loopCounter;
                    m_ListOfActiveExpression[t_SelectedIndex].expressionAttribute.PostProcess(
                        m_ListOfActiveExpression[t_SelectedIndex].customerTransformReference,
                        m_ListOfActiveExpression[t_SelectedIndex].displacement
                        );
                    m_ListOfActiveExpression.RemoveAt(t_SelectedIndex);
                }

                m_ListOfActiveExpression.TrimExcess();
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(ControllerForLifeCycleOfCustomerExpression());

    }

    

    #endregion

    #region Public Callback

    public void PreProcess() {

        
    }

    public void PostProcess() {


    }
    
    public void ShowExpression(float t_Duration, Vector3 t_Displacement, Transform t_CustomerTransformReference, int t_ExpressionId) {

        t_ExpressionId = t_ExpressionId == -1 ? Random.Range(0, customerExpression.Length) : t_ExpressionId;

        ShowExpression(t_Duration, t_Displacement, t_CustomerTransformReference, customerExpression[t_ExpressionId].expressionSprite);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="t_Duration"></param>
    /// <param name="t_Displacement"></param>
    /// <param name="t_CustomerTransformReference"></param>
    /// <param name="t_ExpressionImage"></param>
    public void ShowExpression(float t_Duration, Vector3 t_Displacement, Transform t_CustomerTransformReference, Sprite t_ExpressionImage) {
        
        // if   : anything except '-1' returned, means the expression already assigned for the following customer
        // else : A new expression need to be created
        int t_ListedExpresionIndex = IsExpressionAlreadyCreatedForCustomer(t_CustomerTransformReference);

        if (t_ListedExpresionIndex != -1)
        {
            ActiveExpression t_ModifiedExpression = m_ListOfActiveExpression[t_ListedExpresionIndex];
            t_ModifiedExpression.duration = t_Duration;
            t_ModifiedExpression.expressionSprite = t_ExpressionImage;
            t_ModifiedExpression.expressionAttribute.ChangeExpressionSprite(t_ExpressionImage,true);
            m_ListOfActiveExpression[t_ListedExpresionIndex] = t_ModifiedExpression;
        }
        else
        {

            GameObject t_NewExpressionUI = Instantiate(
                    customerExpressionPrefab,
                    transform.position,
                    Quaternion.identity
                );
            t_NewExpressionUI.transform.SetParent(customerExpressionParent);

            CustomerExpresssionAttribute t_NewExpressionAttriubte = t_NewExpressionUI.GetComponent<CustomerExpresssionAttribute>();
            t_NewExpressionAttriubte.PreProcess(t_ExpressionImage);

            ActiveExpression t_NewExpression = new ActiveExpression()
            {
                duration = t_Duration,
                displacement = t_Displacement,
                customerTransformReference = t_CustomerTransformReference,
                expressionSprite = t_ExpressionImage,
                expressionUI = t_NewExpressionUI.transform,
                expressionAttribute = t_NewExpressionAttriubte
            };

            m_ListOfActiveExpression.Add(t_NewExpression);

            if (!m_IsCustomerExpressionControllerRunning)
            {

                m_IsCustomerExpressionControllerRunning = true;
                StartCoroutine(ControllerForLifeCycleOfCustomerExpression());
            }
        }
    }

    public void DisposeExression(Transform t_CustomerTransformReference) {

        int t_NumberOfActiveExpression = m_ListOfActiveExpression.Count;
        for (int expressionIndex = 0; expressionIndex < t_NumberOfActiveExpression; expressionIndex++) {

            if (m_ListOfActiveExpression[expressionIndex].customerTransformReference == t_CustomerTransformReference) {

                ActiveExpression t_ModifiedActiveExpression = m_ListOfActiveExpression[expressionIndex];
                t_ModifiedActiveExpression.flagedForDispose = true;
                m_ListOfActiveExpression[expressionIndex] = t_ModifiedActiveExpression;

                break;
            }
        }
    }

    public void DisposeAllExpression() {

        int t_NumberOfActiveExpression = m_ListOfActiveExpression.Count;
        for (int expressionIndex = 0; expressionIndex < t_NumberOfActiveExpression; expressionIndex++) {

            ActiveExpression t_ModifiedActiveExpression = m_ListOfActiveExpression[expressionIndex];
            t_ModifiedActiveExpression.flagedForDispose = true;
            m_ListOfActiveExpression[expressionIndex] = t_ModifiedActiveExpression;
        }
    }

    #endregion


    
}
