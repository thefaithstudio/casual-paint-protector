﻿
namespace com.faithstudio.Gameplay
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class DayNightCycleLighting : MonoBehaviour
	{

		public static DayNightCycleLighting Instance;

		#region Public Variables

		[Header("Reference : DirectionalLights")]
		public Light directionalLight;
		[Space(2.5f)]
		public Gradient directionalLightingColorThroughCycle;
		[Space(2.5f)]
		public float directionalLightIntensityMultiplier = 0.35f;
		public AnimationCurve directionalLightIntensityThroughCycle;
		[Space(2.5f)]
		public Vector3 eulerAngleOfDirectionalLightOnSunRise;
		public Vector3 eulerAngleOfDirectionalLightOnSunSet;

		[Space(5.0f)]
		[Header("Reference : Non - DirectionalLights")]
		public Light[] nonDirectionalLights;




		#endregion

		#region Private Variables

		// DIRECTIONAL LIGHT
		private Transform m_TransformReferenceOfDirectionalLight;

		private float m_InitialDirectionalLightIntensity;
		private Keyframe[] m_KeyFrameOfDirectionalLightIntensity;

		private Color m_InitialColorOfDirectionalLight;
		private GradientColorKey[] m_GradientColorKeyForDirectionalLight;



		private bool m_IsDayLightTransitionRunning;

		private float m_DayLength;

		#endregion

		#region Mono Behaviour

		private void Awake()
		{
			if (Instance == null)
			{

				Instance = this;
			}

			m_TransformReferenceOfDirectionalLight = directionalLight.transform;

			m_InitialDirectionalLightIntensity = directionalLight.intensity;
			m_KeyFrameOfDirectionalLightIntensity = directionalLightIntensityThroughCycle.keys;

			for (int index = 0; index < m_KeyFrameOfDirectionalLightIntensity.Length; index++)
			{

				Debug.Log(m_KeyFrameOfDirectionalLightIntensity[index].value + " : " + m_KeyFrameOfDirectionalLightIntensity[index].time);
			}

			m_InitialColorOfDirectionalLight = directionalLight.color;
			m_GradientColorKeyForDirectionalLight = directionalLightingColorThroughCycle.colorKeys;

		}

		private void Start()
		{



		}

		#endregion

		#region Configuretion

		private float GetDirectionalLightIntensityThroughDayCycle(float t_DayCycleProgression)
		{

			float t_IntensityProgression = 0;
			float t_IntesityStart = 0;
			float t_IntensityTarget = m_InitialDirectionalLightIntensity;

			int t_NumberOfIntensityKeyFrameForDirectionalLight = m_KeyFrameOfDirectionalLightIntensity.Length;
			for (int intensityKeyFrameIndex = 0; intensityKeyFrameIndex < t_NumberOfIntensityKeyFrameForDirectionalLight; intensityKeyFrameIndex++)
			{

				if (intensityKeyFrameIndex == 0 && t_DayCycleProgression <= m_KeyFrameOfDirectionalLightIntensity[intensityKeyFrameIndex].time)
				{
					t_IntesityStart = m_InitialDirectionalLightIntensity;
					t_IntensityTarget = m_KeyFrameOfDirectionalLightIntensity[intensityKeyFrameIndex].value * directionalLightIntensityMultiplier;
					t_IntensityProgression = 1f;
					break;
				}
				else if (intensityKeyFrameIndex != 0)
				{

					if (t_DayCycleProgression >= m_KeyFrameOfDirectionalLightIntensity[intensityKeyFrameIndex - 1].time
					&& t_DayCycleProgression < m_KeyFrameOfDirectionalLightIntensity[intensityKeyFrameIndex].time)
					{
						t_IntesityStart = m_KeyFrameOfDirectionalLightIntensity[intensityKeyFrameIndex - 1].value * directionalLightIntensityMultiplier;
						t_IntensityTarget = m_KeyFrameOfDirectionalLightIntensity[intensityKeyFrameIndex].value * directionalLightIntensityMultiplier;
						t_IntensityProgression = (t_DayCycleProgression - m_KeyFrameOfDirectionalLightIntensity[intensityKeyFrameIndex - 1].time) / (m_KeyFrameOfDirectionalLightIntensity[intensityKeyFrameIndex].time - m_KeyFrameOfDirectionalLightIntensity[intensityKeyFrameIndex - 1].time);

						break;
					}
				}
			}

			return Mathf.Lerp(
					t_IntesityStart,
					t_IntensityTarget,
					t_IntensityProgression
				);
		}

		private Color GetDirectionalLightColorThroughDayCycle(float t_DayCycleProgression)
		{

			float t_ColorProgression = 0;
			Color t_ColorStart = m_InitialColorOfDirectionalLight;
			Color t_ColorTarget = m_InitialColorOfDirectionalLight;

			int t_NumberOfGradientColorKeyForDirectionalLight = m_GradientColorKeyForDirectionalLight.Length;
			for (int gradientColorKeyIndex = 0; gradientColorKeyIndex < t_NumberOfGradientColorKeyForDirectionalLight; gradientColorKeyIndex++)
			{

				if (gradientColorKeyIndex == 0 && t_DayCycleProgression <= m_GradientColorKeyForDirectionalLight[gradientColorKeyIndex].time)
				{
					t_ColorStart = m_InitialColorOfDirectionalLight;
					t_ColorTarget = m_GradientColorKeyForDirectionalLight[gradientColorKeyIndex].color;
					t_ColorProgression = 1f;
					break;
				}
				else if (gradientColorKeyIndex != 0)
				{

					if (t_DayCycleProgression >= m_GradientColorKeyForDirectionalLight[gradientColorKeyIndex - 1].time
					&& t_DayCycleProgression < m_GradientColorKeyForDirectionalLight[gradientColorKeyIndex].time)
					{

						t_ColorStart = m_GradientColorKeyForDirectionalLight[gradientColorKeyIndex - 1].color;
						t_ColorTarget = m_GradientColorKeyForDirectionalLight[gradientColorKeyIndex].color;
						t_ColorProgression = (t_DayCycleProgression - m_GradientColorKeyForDirectionalLight[gradientColorKeyIndex - 1].time) / (m_GradientColorKeyForDirectionalLight[gradientColorKeyIndex].time - m_GradientColorKeyForDirectionalLight[gradientColorKeyIndex - 1].time);
						break;
					}
				}
			}

			return Color.Lerp(
					t_ColorStart,
					t_ColorTarget,
					t_ColorProgression
				);
		}

		private IEnumerator ControllerForDayLightTransition()
		{

			float t_CycleLength = 0.0167f;
			WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

			float t_RemainingTimeOfDayNightCycle = m_DayLength;
			float t_Progression;

			Vector3 t_ModifiedEulerAngleOfLight;

			while (t_RemainingTimeOfDayNightCycle > 0)
			{

				t_Progression = 1f - (t_RemainingTimeOfDayNightCycle / m_DayLength);

				t_ModifiedEulerAngleOfLight = Vector3.Lerp(
						eulerAngleOfDirectionalLightOnSunRise,
						eulerAngleOfDirectionalLightOnSunSet,
						t_Progression
					);
				m_TransformReferenceOfDirectionalLight.localEulerAngles = t_ModifiedEulerAngleOfLight;

				directionalLight.color = GetDirectionalLightColorThroughDayCycle(t_Progression);
				directionalLight.intensity = GetDirectionalLightIntensityThroughDayCycle(t_Progression);

				t_RemainingTimeOfDayNightCycle -= t_CycleLength;
				yield return t_CycleDelay;
			}

			m_IsDayLightTransitionRunning = false;
			StopCoroutine(ControllerForDayLightTransition());
		}

		#endregion

		#region Public Callback :   Time

		public bool IsDayLightTransitionAlreadyRunning()
		{
			return m_IsDayLightTransitionRunning;
		}

		public void StartDayLightTransition(float t_DayLength)
		{

			if (!IsDayLightTransitionAlreadyRunning())
			{

				m_DayLength = t_DayLength;

				m_IsDayLightTransitionRunning = true;
				StartCoroutine(ControllerForDayLightTransition());
			}
			else
			{
				Debug.LogError("DayLight Transtion already running");
			}
		}

		#endregion

		#region Public Callback :   Liner Interpolation

		public void ResetDayLightTransitionAsLinearInterpolation()
		{
			m_TransformReferenceOfDirectionalLight.localEulerAngles = eulerAngleOfDirectionalLightOnSunRise;
			directionalLight.color = GetDirectionalLightColorThroughDayCycle(0);
			directionalLight.intensity = GetDirectionalLightIntensityThroughDayCycle(0);
		}

		public void DayLightTransitionAsLinearInterpolation(float t_Progression)
		{

			Vector3 t_ModifiedEulerAngleOfLight = Vector3.Lerp(
						eulerAngleOfDirectionalLightOnSunRise,
						eulerAngleOfDirectionalLightOnSunSet,
						t_Progression
					);
			m_TransformReferenceOfDirectionalLight.localEulerAngles = t_ModifiedEulerAngleOfLight;

			directionalLight.color = GetDirectionalLightColorThroughDayCycle(t_Progression);
			directionalLight.intensity = GetDirectionalLightIntensityThroughDayCycle(t_Progression);
		}

		#endregion
	}
}


