﻿namespace com.faithstudio.Gameplay {

    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class CoinEarningController : MonoBehaviour
    {

        #region Custom Variables

        private struct CoinInfo
        {

            public bool isInitialDisplacementComplete;
            public double amountOfCoinToBeAdded;
            public float initialDistanceFromDisplacement;
            public float initialDistanceFromDisplacementToTarget;
            public Transform coinContainerTransformReference;
            public Vector3 initialPositionOfDisplacement;

        }

        #endregion

        #region Public Variables

        public static CoinEarningController Instance;

        public PreLoadedPrefab  coinContainer;

        [Space(5.0f)]
        public bool isMoveTowardsHUD;
        public Transform        coinHUDTransformReference;
        public Vector3 animationPosition;

        [Range(0f,100f)]
        public float coinMovementSpeed = 10;
        [Range(0f,90f)]
        public float coinRotationSpeed = 30;
        [Space(5.0f)]
        [Header("(Optional) : Monetization")]
        public GlobalMonetizationStateController globalMonetizationStateControllerReference;

        #endregion

        #region Private Variables

        private bool m_IsCoinEarnControllerRunning;
        private bool m_IsCoinEarnAllow;

        private float m_BufferTimeForCoinEarn;
        private float m_CurrentBufferTimeForCoinEarn;

        private double m_NumberOfCoinEarnedOnThisLevel;
        private double m_NumberOfCoinStackedToUpdateInUI;
        private double m_CoinEarnStateForLevel;

        private List<CoinInfo> m_ListOfCoinInfo;

        #endregion

        #region Mono Behaviour

        private void Awake()
        {
            Instance = this;
        }

        #endregion

        #region Configuretion

        private IEnumerator ControllerForCoinEarn()
        {
            float t_DeltaTime;
            float t_CycleLength = 0.0167f;
            WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

            int t_NumberOfAvailableCoinInfo = 0;
            float t_CoinPositionCheckBoundary = coinMovementSpeed * 0.025f;
            float t_ScaleValue;
            float t_CurrentDistance;
            Vector3 t_ZeroScale = Vector3.zero;
            Vector3 t_OneScale = Vector3.one;
            Vector3 t_TargetedPosition;
            Vector3 t_ModifiedPosition;
            Vector3 t_ModifiedScale;
            Vector3 t_RandomRotation;
            CoinInfo t_SelectedCoinInfoReference;

            t_RandomRotation = new Vector3(
                        Random.Range(-1f, 1f),
                        Random.Range(-1f, 1f),
                        Random.Range(-1f, 1f)
                    );

            while (m_IsCoinEarnControllerRunning || t_NumberOfAvailableCoinInfo > 0)
            {

                t_DeltaTime = Time.deltaTime;

                if (!m_IsCoinEarnAllow
                && m_CurrentBufferTimeForCoinEarn <= 0)
                {

                    m_IsCoinEarnAllow = true;
                }
                else
                {

                    m_CurrentBufferTimeForCoinEarn -= t_CycleLength;
                }

                t_NumberOfAvailableCoinInfo = m_ListOfCoinInfo.Count;
                for (int counter = 0; counter < t_NumberOfAvailableCoinInfo; counter++)
                {

                    t_SelectedCoinInfoReference = m_ListOfCoinInfo[counter];
                    if (!t_SelectedCoinInfoReference.isInitialDisplacementComplete)
                    {

                        t_ModifiedPosition = Vector3.MoveTowards(
                                t_SelectedCoinInfoReference.coinContainerTransformReference.position,
                                t_SelectedCoinInfoReference.initialPositionOfDisplacement,
                                coinMovementSpeed * t_DeltaTime
                            );
                        t_SelectedCoinInfoReference.coinContainerTransformReference.Rotate(t_RandomRotation * coinRotationSpeed);


                        t_CurrentDistance = Vector3.Distance(t_SelectedCoinInfoReference.initialPositionOfDisplacement, t_ModifiedPosition);

                        t_ModifiedScale = Vector3.Lerp(
                                t_ZeroScale,
                                t_OneScale,
                                1f - (t_CurrentDistance / t_SelectedCoinInfoReference.initialDistanceFromDisplacement)
                            );

                        t_SelectedCoinInfoReference.coinContainerTransformReference.localScale = t_ModifiedScale;

                        if (t_CurrentDistance <= t_CoinPositionCheckBoundary)
                        {

                            t_SelectedCoinInfoReference.coinContainerTransformReference.position = t_SelectedCoinInfoReference.initialPositionOfDisplacement;
                            t_SelectedCoinInfoReference.coinContainerTransformReference.localScale = t_OneScale;
                            t_SelectedCoinInfoReference.initialDistanceFromDisplacementToTarget = isMoveTowardsHUD ? Vector3.Distance(coinHUDTransformReference.position, t_ModifiedPosition) : Vector3.Distance(t_ModifiedPosition + animationPosition, t_ModifiedPosition);
                            t_SelectedCoinInfoReference.isInitialDisplacementComplete = true;
                            m_ListOfCoinInfo[counter] = t_SelectedCoinInfoReference;
                        }
                        else
                        {

                            t_SelectedCoinInfoReference.coinContainerTransformReference.position = t_ModifiedPosition;
                        }
                    }
                    else
                    {

                        if (isMoveTowardsHUD)
                            t_TargetedPosition = coinHUDTransformReference.position;
                        else
                            t_TargetedPosition = t_SelectedCoinInfoReference.initialPositionOfDisplacement + animationPosition;

                        t_ModifiedPosition = Vector3.MoveTowards(
                                t_SelectedCoinInfoReference.coinContainerTransformReference.position,
                                t_TargetedPosition,
                                coinMovementSpeed * 2 * t_DeltaTime
                            );

                        t_CurrentDistance = Vector3.Distance(t_TargetedPosition, t_ModifiedPosition);
                        t_ScaleValue = t_CurrentDistance / t_SelectedCoinInfoReference.initialDistanceFromDisplacementToTarget;
                        t_ScaleValue = t_ScaleValue > 1 ? 0f : 1f - t_ScaleValue;

                        t_ModifiedScale = Vector3.Lerp(
                                t_OneScale,
                                t_ZeroScale,
                                t_ScaleValue
                         );

                        if (t_CurrentDistance <= t_CoinPositionCheckBoundary)
                        {

                            t_SelectedCoinInfoReference.coinContainerTransformReference.position = coinHUDTransformReference.position;
                            t_SelectedCoinInfoReference.coinContainerTransformReference.gameObject.SetActive(false);

                            m_NumberOfCoinEarnedOnThisLevel += t_SelectedCoinInfoReference.amountOfCoinToBeAdded;

                            if (isMoveTowardsHUD) {

                                AudioController.Instance.PlaySoundFXForCoinEarn();
                                UIStateController.Instance.UpdateUIForCoinEarn(m_NumberOfCoinEarnedOnThisLevel);
                                GameManager.Instance.UpdateInGameCurrency(t_SelectedCoinInfoReference.amountOfCoinToBeAdded);
                            }
                            

                            m_ListOfCoinInfo.RemoveAt(counter);
                            t_NumberOfAvailableCoinInfo--;

                            coinContainer.PushPreloadedPrefab(t_SelectedCoinInfoReference.coinContainerTransformReference.gameObject);
                        }
                        else
                        {

                            t_SelectedCoinInfoReference.coinContainerTransformReference.position = t_ModifiedPosition;
                            t_SelectedCoinInfoReference.coinContainerTransformReference.localScale = t_ModifiedScale;
                            t_SelectedCoinInfoReference.coinContainerTransformReference.Rotate(t_RandomRotation * coinRotationSpeed);

                        }
                    }
                }

                yield return t_CycleDelay;
            }

            StopCoroutine(ControllerForCoinEarn());

        }

        #endregion

        #region Public Callback

        public void PreProcess(double t_CoinEarnStateForLevel, float t_UpgradeProgressionOfCoinEarn = 0)
        {

            m_CoinEarnStateForLevel = t_CoinEarnStateForLevel;
            m_BufferTimeForCoinEarn = 0.1f - (0.09f * t_UpgradeProgressionOfCoinEarn);


            if (!m_IsCoinEarnControllerRunning)
            {
                m_NumberOfCoinEarnedOnThisLevel = 0;
                m_NumberOfCoinStackedToUpdateInUI = 0;
                m_IsCoinEarnControllerRunning = true;
                m_IsCoinEarnAllow = true;
                m_ListOfCoinInfo = new List<CoinInfo>();
                StartCoroutine(ControllerForCoinEarn());

            }
        }

        public void PostProcess()
        {

            m_IsCoinEarnControllerRunning = false;
        }

        public void AddCoinToUser(Vector3 t_CoinSpawnPosition, Vector3 t_InitialPositionDisplacement, Vector3 t_InitialDirection, float t_Multiplier = 1f)
        {
            double t_CoinToBeAdded              = m_CoinEarnStateForLevel * t_Multiplier * (globalMonetizationStateControllerReference != null ? (globalMonetizationStateControllerReference.IsCoinEarnBoostEnabled() ? 2f : 1f) : 1f);
            m_NumberOfCoinStackedToUpdateInUI   += t_CoinToBeAdded;

            if (!isMoveTowardsHUD)
            {
                m_NumberOfCoinEarnedOnThisLevel += t_CoinToBeAdded;

                AudioController.Instance.PlaySoundFXForCoinEarn();
                UIStateController.Instance.UpdateUIForCoinEarn(m_NumberOfCoinEarnedOnThisLevel);
                GameManager.Instance.UpdateInGameCurrency(t_CoinToBeAdded);
            }

            if (m_IsCoinEarnAllow)
            {
                GameObject t_NewCoin = coinContainer.PullPreloadedPrefab();
                Transform t_NewCoinTransform = t_NewCoin.transform;
                t_NewCoinTransform.position = t_CoinSpawnPosition;
                t_NewCoinTransform.eulerAngles = t_InitialDirection;
                t_NewCoinTransform.localScale = Vector3.zero;

                CoinInfo t_NewCoinInfo = new CoinInfo()
                {
                    isInitialDisplacementComplete = false,
                    amountOfCoinToBeAdded = m_NumberOfCoinStackedToUpdateInUI,
                    coinContainerTransformReference = t_NewCoinTransform,
                    initialPositionOfDisplacement = t_CoinSpawnPosition + t_InitialPositionDisplacement,
                    initialDistanceFromDisplacement =  Vector3.Distance(t_CoinSpawnPosition, t_CoinSpawnPosition + t_InitialPositionDisplacement)
                };

                m_ListOfCoinInfo.Add(t_NewCoinInfo);


                m_CurrentBufferTimeForCoinEarn = m_BufferTimeForCoinEarn;
                m_NumberOfCoinStackedToUpdateInUI = 0;
                m_IsCoinEarnAllow = false;
            }
        }

        public double GetNumberOfCoinEarnedInThisLevel() {

            return m_NumberOfCoinEarnedOnThisLevel;
        }

        #endregion
    }

}

